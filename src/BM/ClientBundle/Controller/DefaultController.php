<?php

namespace BM\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BMClientBundle:Default:index.html.twig');
    }
}
