<?php

namespace BM\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use BM\SalesBundle\Entity\SalesDocument;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Client
 *
 * @ORM\Table(name="client")
 * @ORM\Entity(repositoryClass="BM\ClientBundle\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numcl", type="string", length=255)
     */
    private $numcl;

    /**
     * @var string
     *
     * @ORM\Column(name="nomcl", type="string", length=255)
     */
    private $nomcl;

    /**
     * @var string
     *
     * @ORM\Column(name="raisonsocialcl", type="string", length=255)
     */
    private $raisonsocialcl;

    /**
     * @var string
     *
     * @ORM\Column(name="adressecl", type="string", length=255)
     */
    private $adressecl;

    /**
     * @var string
     *
     * @ORM\Column(name="codepostalcl", type="string", length=255)
     */
    private $codepostalcl;

    /**
     * @var string
     *
     * @ORM\Column(name="villecl", type="string", length=255)
     */
    private $villecl;

    /**
     * @var string
     *
     * @ORM\Column(name="payscl", type="string", length=255)
     */
    private $payscl;

    /**
     * @var string
     *
     * @ORM\Column(name="telcl", type="string", length=255)
     */
    private $telcl;

    /**
     * @var string
     *
     * @ORM\Column(name="emailcl", type="string", length=255)
     */
    private $emailcl;

    /**
     * @var string
     *
     * @ORM\Column(name="matriculeFiscalCl", type="string", length=255)
     */
    private $matriculeFiscalCl;

    /**
     * @var string
     *
     * @ORM\Column(name="NExonerationFiscal", type="string", length=255)
     */
    private $nExonerationFiscal;

    /**
     * @var string
     *
     * @ORM\Column(name="codeDouaneCl", type="string", length=255)
     */
    private $codeDouaneCl;

    /**
     * @var string
     *
     * @ORM\Column(name="ExoTimbreFiscalCl", type="string", length=255)
     */
    private $exoTimbreFiscalCl;

    /**
     * @var string
     *
     * @ORM\Column(name="TypecommercialeCl", type="string", length=255)
     */
    private $typecommercialeCl;

    /**
     * @var string
     *
     * @ORM\Column(name="remiseCl", type="string", length=255)
     */
    private $remiseCl;

    /**
     * @var string
     *
     * @ORM\Column(name="faxCl", type="string", length=255)
     */
    private $faxCl;

    /**
    * @ORM\OneToMany(targetEntity="BM\SalesBundle\Entity\SalesDocument", mappedBy="client")
    */
   private $salesDocuments ;
    public function __construct(){
       $this-> salesDocuments = new ArrayCollection();
       
   }





    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numcl
     *
     * @param string $numcl
     *
     * @return Client
     */
    public function setNumcl($numcl)
    {
        $this->numcl = $numcl;

        return $this;
    }

    /**
     * Get numcl
     *
     * @return string
     */
    public function getNumcl()
    {
        return $this->numcl;
    }

    /**
     * Set nomcl
     *
     * @param string $nomcl
     *
     * @return Client
     */
    public function setNomcl($nomcl)
    {
        $this->nomcl = $nomcl;

        return $this;
    }

    /**
     * Get nomcl
     *
     * @return string
     */
    public function getNomcl()
    {
        return $this->nomcl;
    }

    /**
     * Set raisonsocialcl
     *
     * @param string $raisonsocialcl
     *
     * @return Client
     */
    public function setRaisonsocialcl($raisonsocialcl)
    {
        $this->raisonsocialcl = $raisonsocialcl;

        return $this;
    }

    /**
     * Get raisonsocialcl
     *
     * @return string
     */
    public function getRaisonsocialcl()
    {
        return $this->raisonsocialcl;
    }

    /**
     * Set adressecl
     *
     * @param string $adressecl
     *
     * @return Client
     */
    public function setAdressecl($adressecl)
    {
        $this->adressecl = $adressecl;

        return $this;
    }

    /**
     * Get adressecl
     *
     * @return string
     */
    public function getAdressecl()
    {
        return $this->adressecl;
    }

    /**
     * Set codepostalcl
     *
     * @param string $codepostalcl
     *
     * @return Client
     */
    public function setCodepostalcl($codepostalcl)
    {
        $this->codepostalcl = $codepostalcl;

        return $this;
    }

    /**
     * Get codepostalcl
     *
     * @return string
     */
    public function getCodepostalcl()
    {
        return $this->codepostalcl;
    }

    /**
     * Set villecl
     *
     * @param string $villecl
     *
     * @return Client
     */
    public function setVillecl($villecl)
    {
        $this->villecl = $villecl;

        return $this;
    }

    /**
     * Get villecl
     *
     * @return string
     */
    public function getVillecl()
    {
        return $this->villecl;
    }

    /**
     * Set payscl
     *
     * @param string $payscl
     *
     * @return Client
     */
    public function setPayscl($payscl)
    {
        $this->payscl = $payscl;

        return $this;
    }

    /**
     * Get payscl
     *
     * @return string
     */
    public function getPayscl()
    {
        return $this->payscl;
    }

    /**
     * Set telcl
     *
     * @param string $telcl
     *
     * @return Client
     */
    public function setTelcl($telcl)
    {
        $this->telcl = $telcl;

        return $this;
    }

    /**
     * Get telcl
     *
     * @return string
     */
    public function getTelcl()
    {
        return $this->telcl;
    }

    /**
     * Set emailcl
     *
     * @param string $emailcl
     *
     * @return Client
     */
    public function setEmailcl($emailcl)
    {
        $this->emailcl = $emailcl;

        return $this;
    }

    /**
     * Get emailcl
     *
     * @return string
     */
    public function getEmailcl()
    {
        return $this->emailcl;
    }

    /**
     * Set matriculeFiscalCl
     *
     * @param string $matriculeFiscalCl
     *
     * @return Client
     */
    public function setMatriculeFiscalCl($matriculeFiscalCl)
    {
        $this->matriculeFiscalCl = $matriculeFiscalCl;

        return $this;
    }

    /**
     * Get matriculeFiscalCl
     *
     * @return string
     */
    public function getMatriculeFiscalCl()
    {
        return $this->matriculeFiscalCl;
    }

    /**
     * Set nExonerationFiscal
     *
     * @param string $nExonerationFiscal
     *
     * @return Client
     */
    public function setNExonerationFiscal($nExonerationFiscal)
    {
        $this->nExonerationFiscal = $nExonerationFiscal;

        return $this;
    }

    /**
     * Get nExonerationFiscal
     *
     * @return string
     */
    public function getNExonerationFiscal()
    {
        return $this->nExonerationFiscal;
    }

    /**
     * Set codeDouaneCl
     *
     * @param string $codeDouaneCl
     *
     * @return Client
     */
    public function setCodeDouaneCl($codeDouaneCl)
    {
        $this->codeDouaneCl = $codeDouaneCl;

        return $this;
    }

    /**
     * Get codeDouaneCl
     *
     * @return string
     */
    public function getCodeDouaneCl()
    {
        return $this->codeDouaneCl;
    }

    /**
     * Set exoTimbreFiscalCl
     *
     * @param string $exoTimbreFiscalCl
     *
     * @return Client
     */
    public function setExoTimbreFiscalCl($exoTimbreFiscalCl)
    {
        $this->exoTimbreFiscalCl = $exoTimbreFiscalCl;

        return $this;
    }

    /**
     * Get exoTimbreFiscalCl
     *
     * @return string
     */
    public function getExoTimbreFiscalCl()
    {
        return $this->exoTimbreFiscalCl;
    }

    /**
     * Set typecommercialeCl
     *
     * @param string $typecommercialeCl
     *
     * @return Client
     */
    public function setTypecommercialeCl($typecommercialeCl)
    {
        $this->typecommercialeCl = $typecommercialeCl;

        return $this;
    }

    /**
     * Get typecommercialeCl
     *
     * @return string
     */
    public function getTypecommercialeCl()
    {
        return $this->typecommercialeCl;
    }

    /**
     * Set remiseCl
     *
     * @param string $remiseCl
     *
     * @return Client
     */
    public function setRemiseCl($remiseCl)
    {
        $this->remiseCl = $remiseCl;

        return $this;
    }

    /**
     * Get remiseCl
     *
     * @return string
     */
    public function getRemiseCl()
    {
        return $this->remiseCl;
    }

    /**
     * Set faxCl
     *
     * @param string $faxCl
     *
     * @return Client
     */
    public function setFaxCl($faxCl)
    {
        $this->faxCl = $faxCl;

        return $this;
    }

    /**
     * Get faxCl
     *
     * @return string
     */
    public function getFaxCl()
    {
        return $this->faxCl;
    }








    /**
    * 
    * addSalesDocument()
    */
   public function addSalesDocument(SalesDocument $salesDocument ){
       $this->salesDocuments->add($salesDocument);
       return $this;
   }
   /**
    * 
    * removeSalesDocument()
    */
   public function removeSalesDocument(SalesDocument $salesDocument ){
       $this->salesDocuments->removeElement($salesDocument);
       return $this;
   }

   /**
    * 
    * getSalesDocuments()
    */
   public function getSalesDocuments(){
       return $this->salesDocuments;
   }

   public function __toString() {
    return $this->nomcl;
    }
}

