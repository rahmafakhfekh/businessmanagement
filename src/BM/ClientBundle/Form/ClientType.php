<?php

namespace BM\ClientBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('numcl')->add('nomcl')->add('raisonsocialcl')->add('adressecl')->add('codepostalcl')->add('villecl')->add('payscl')->add('telcl')->add('emailcl')->add('matriculeFiscalCl')->add('nExonerationFiscal')->add('codeDouaneCl')->add('exoTimbreFiscalCl')->add('typecommercialeCl')->add('remiseCl')->add('faxCl');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BM\ClientBundle\Entity\Client'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'bm_clientbundle_client';
    }


}
