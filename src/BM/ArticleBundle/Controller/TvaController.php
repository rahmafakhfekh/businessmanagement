<?php

namespace BM\ArticleBundle\Controller;

use BM\ArticleBundle\Entity\tva;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Tva controller.
 *
 * @Route("tva")
 */
class TvaController extends Controller
{
    /**
     * Lists all tva entities.
     *
     * @Route("/", name="tva_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $tvas = $em->getRepository('BMArticleBundle:tva')->findAll();

 $dql   = "SELECT a FROM BMArticleBundle:tva a";
    $query = $em->createQuery($dql);

       /**
        *@var $paginator \Knp\Component\Pager\Paginator
        */

  $paginator  = $this->get('knp_paginator');
    $result = $paginator->paginate(
        $query, /* query NOT result */
        $request->query->getInt('page', 1)/*page number*/,
        $request->query->getInt('limit', 10)/*limit per page*/ 
);




        return $this->render('tva/index.html.twig', array(
            'tvas' => $result,
        ));
    }

    /**
     * Creates a new tva entity.
     *
     * @Route("/new", name="tva_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $tva = new Tva();
        $form = $this->createForm('BM\ArticleBundle\Form\tvaType', $tva);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tva);
            $em->flush();

            return $this->redirectToRoute('tva_index', array('id' => $tva->getId()));
        }

        return $this->render('tva/new.html.twig', array(
            'tva' => $tva,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a tva entity.
     *
     * @Route("/{id}", name="tva_show")
     * @Method("GET")
     */
    public function showAction(tva $tva)
    {
        $deleteForm = $this->createDeleteForm($tva);

        return $this->render('tva/show.html.twig', array(
            'tva' => $tva,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing tva entity.
     *
     * @Route("/{id}/edit", name="tva_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, tva $tva)
    {
        $deleteForm = $this->createDeleteForm($tva);
        $editForm = $this->createForm('BM\ArticleBundle\Form\tvaType', $tva);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tva_index', array('id' => $tva->getId()));
        }

        return $this->render('tva/edit.html.twig', array(
            'tva' => $tva,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a tva entity.
     *
     * @Route("/{id}", name="tva_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, tva $tva)
    {
        $form = $this->createDeleteForm($tva);
        $form->handleRequest($request);

        //if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tva);
            $em->flush();
        //}

        return $this->redirectToRoute('tva_index');
    }

    /**
     * Creates a form to delete a tva entity.
     *
     * @param tva $tva The tva entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(tva $tva)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tva_delete', array('id' => $tva->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
