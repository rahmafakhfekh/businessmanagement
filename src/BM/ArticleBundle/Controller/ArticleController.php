<?php

namespace BM\ArticleBundle\Controller;

use BM\ArticleBundle\Entity\Article;
use BM\ArticleBundle\Entity\Stock;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * Article controller.
 *
 * @Route("article")
 */
class ArticleController extends Controller
{
    /**
     * Lists all article entities.
     *
     * @Route("/", name="article_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
  
        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository('BMArticleBundle:Article')->findAll();

            $dql   = "SELECT a FROM BMArticleBundle:Article a";
    $query = $em->createQuery($dql);

       /**
        *@var $paginator \Knp\Component\Pager\Paginator
        */

  $paginator  = $this->get('knp_paginator');
    $result = $paginator->paginate(
        $query, /* query NOT result */
        $request->query->getInt('page', 1)/*page number*/,
        $request->query->getInt('limit', 10)/*limit per page*/ 
);


        return $this->render('article/index.html.twig', array(
            'articles' => $result,
        ));
    }
    /**
     * Lists all article entities.
     *
     * @Route("/", name="article_index2")
     * @Method("GET")
     */
    public function printAction(Request $request)
    {
  
        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository('BMArticleBundle:Article')->findAll();

        return $this->render('article/print.html.twig', array(
            'articles' => $articles,
        ));
    }
    public function getDataAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('BMArticleBundle:Article')->findOneBy(array('id' =>$id)); 
        $result['quantity'] = $article->getStock()->getQt();
        $result['priceVDHT'] = $article->getPriceVDHT();
        $result['priceVDTTC'] = $article->getPriceVDTTC();
        $result['priceVGHT'] = $article->getPriceVGHT();
        $result['priceVGTTC'] = $article->getPriceVGTTC();
        if ($article->getTva() != null)
            $result['tva'] = $article->getTva()->getPourcentageTva();
        else
            $result['tva'] = '0';

        return JsonResponse::create($result);
    }
    /**
     * Creates a new article entity.
     *
     * @Route("/new", name="article_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $article = new Article();
        $form = $this->createForm('BM\ArticleBundle\Form\ArticleType', $article);
        $form->handleRequest($request);
        $requesParams = $request->request->get('bm_articlebundle_article');
      
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $tva1 = $requesParams['tva'];
            $tvaEntity = $em->getRepository('BMArticleBundle:Tva')->findOneBy(array('id' => $tva1));
            if ($tvaEntity == null)
                $tva = 0;
            else
                $tva = $tvaEntity->getPourcentageTva();

            $priceVDHT = $requesParams['priceVDHT'];
            $priceVGHT = $requesParams['priceVGHT'];
            $priceAHT = $requesParams['priceAHT'];
            $priceVDTTC = 0;
            $priceVGTTC = 0;
            $priceATTC = 0;
            if ($priceVGHT == '')
                $article->setPriceVGHT('0');
            if ($priceAHT == '')
                $article->setPriceAHT('0');
            $priceVDTTC = $priceVDHT * (1 +($tva/100));
            $article->setPriceVDTTC($priceVDTTC);
            $priceVGTTC = $priceVGHT * (1 +($tva/100));
            $article->setPriceVGTTC($priceVGTTC);
            $priceATTC = $priceAHT * (1 +($tva/100));
            $article->setPriceATTC($priceATTC);
            $em->persist($article);
           
            
            $stock = new Stock();
            $stock->setArticle($article);
            $stock->setQT($article->getQt());

            
            
            $em->persist($stock);

  
            $em->flush();
            return $this->redirectToRoute('article_index');



           // return $this->redirectToRoute('article_show', array('id' => $article->getId()));
        }
        // else {
        //     $errors = $this->get('validator')->validate( $article );
        
        //    $errorArray =  array();
        //    foreach( $errors as $error )
        //    {
        //        $errorArray[ $error->getPropertyPath() ] =  $error->getMessage();
        //    }
        //    var_dump($form->getErrors());die;
        //    print_r($errorArray);die;
            
        // }

        return $this->render('article/new.html.twig', array(
            'article' => $article,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a article entity.
     *
     * @Route("/{id}", name="article_show")
     * @Method("GET")
     */
    public function showAction(Article $article)
    {
        $deleteForm = $this->createDeleteForm($article);

        return $this->render('article/show.html.twig', array(
            'article' => $article,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing article entity.
     *
     * @Route("/{id}/edit", name="article_edit")
     * @Method({"GET", "POST"})
     */
  public function editAction(Request $request, Article $article)
    {
        $deleteForm = $this->createDeleteForm($article);
        $editForm = $this->createForm('BM\ArticleBundle\Form\ArticleType', $article);
        $editForm->handleRequest($request);
        $requesParams = $request->request->get('bm_articlebundle_article');

        if ($editForm->isSubmitted() && $editForm->isValid()) {
             $em = $this->getDoctrine()->getManager();
            $tva1 = $requesParams['tva'];
            $tvaEntity = $em->getRepository('BMArticleBundle:Tva')->findOneBy(array('id' => $tva1));
            if ($tvaEntity == null)
                $tva = 0;
            else
                $tva = $tvaEntity->getPourcentageTva();

            $priceVDHT = $requesParams['priceVDHT'];
            $priceVGHT = $requesParams['priceVGHT'];
            $priceAHT = $requesParams['priceAHT'];
            $priceVDTTC = 0;
            $priceVGTTC = 0;
            $priceATTC = 0;
            if ($priceVGHT == '')
                $article->setPriceVGHT('0');
            if ($priceAHT == '')
                $article->setPriceAHT('0');
            $priceVDTTC = $priceVDHT * (1 +($tva/100));
            $article->setPriceVDTTC($priceVDTTC);
            $priceVGTTC = $priceVGHT * (1 +($tva/100));
            $article->setPriceVGTTC($priceVGTTC);
            $priceATTC = $priceAHT * (1 +($tva/100));
            $article->setPriceATTC($priceATTC);
            $em->persist($article);

            $stock = $em->getRepository('BMArticleBundle:Stock')->findOneBy(array('article' => $article));
            if ($stock == null)
                $stock = new Stock();
            $stock->setArticle($article);
            $stock->setQT($article->getQt());


             $em->persist($stock);



            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('article_index', array('id' => $article->getId()));
        }

        return $this->render('article/edit.html.twig', array(
            'article' => $article,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a article entity.
     *
     * @Route("/{id}", name="article_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Article $article)
    {
        $form = $this->createDeleteForm($article);
        $form->handleRequest($request);

        //if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush();
       // }

        return $this->redirectToRoute('article_index');
    }

    /**
     * Creates a form to delete a article entity.
     *
     * @param Article $article The article entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Article $article)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('article_delete', array('id' => $article->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


}
