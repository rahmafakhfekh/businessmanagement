<?php

namespace BM\ArticleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class ArticleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
      public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('reference')->add('designation')
        //->add('priceVDHT')
        ->add('priceVDHT', NumberType::class, array(
            'required' => true,
            'attr' => array(
                                
                               ),
            ))
        ->add('priceVDTTC', NumberType::class, array(
            'required' => false,
            'attr' => array(
                 'disabled'  => true,              
                               ),
            ))
        ->add('priceVGHT', NumberType::class, array(
            'required' => false,
            'attr' => array(
                                
                               ),
            ))
        ->add('priceVGTTC', NumberType::class, array(
            'required' => false,
            'attr' => array(
                 'disabled'  => true,              
                                
                               ),
            ))
        ->add('priceAHT', NumberType::class, array(
            'required' => false,
            'attr' => array(
                                
                               ),
            ))
        ->add('priceATTC', NumberType::class, array(
            'required' => false,
            'attr' => array(
                 'disabled'  => true,              
                                
                               ),
            ))
        ->add('status')
        ->add('cut', TextType::class, array(
            'required' => false,
            'attr' => array(
                                
                               ),
            ))
        ->add('color', TextType::class, array(
            'required' => false,
            'attr' => array(
                                
                               ),
            ))
        ->add('height', NumberType::class, array(
            'required' => false,
            'attr' => array(
                                
                               ),
            ))
        ->add('width', NumberType::class, array(
            'required' => false,
            'attr' => array(
                                
                               ),
            ))
        ->add('length', NumberType::class, array(
            'required' => false,
            'attr' => array(
                                
                               ),
            ))
        ->add('weight', NumberType::class, array(
            'required' => false,
            'attr' => array(
                                
                               ),
            ))

         ->add('Qt', NumberType::class, array(
            'required' => True,
            'attr' => array(
                                
                               ),
            ))
        ->add('category')->add('tva')
        ->add('barCode', TextType::class, array(
            'required' => false,
            'attr' => array(
                                
                               ),
            ))
        ;
    }
   /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BM\ArticleBundle\Entity\Article'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'bm_articlebundle_article';
    }


}
