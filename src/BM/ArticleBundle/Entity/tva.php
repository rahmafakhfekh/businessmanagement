<?php

namespace BM\ArticleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use BM\ArticleBundle\Entity\Article;


/**
 * tva
 *
 * @ORM\Table(name="tva")
 * @ORM\Entity(repositoryClass="BM\ArticleBundle\Repository\tvaRepository")
 */
class tva
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pourcentageTva", type="string", length=255)
     */
    private $pourcentageTva;
  /**
    * @ORM\OneToMany(targetEntity="BM\ArticleBundle\Entity\Article", mappedBy="tva")
    */
   private $articles;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pourcentageTva
     *
     * @param string $pourcentageTva
     *
     * @return tva
     */
    public function setPourcentageTva($pourcentageTva)
    {
        $this->pourcentageTva = $pourcentageTva;

        return $this;
    }

    /**
     * Get pourcentageTva
     *
     * @return string
     */
    public function getPourcentageTva()
    {
        return $this->pourcentageTva;
    }
    
    /**
    * 
    * addArticle()
    */
   public function addArticle(Article $article ){
       $this->articles->add($article);
       return $this;
   }
   /**
    * 
    * removeArticle()
    */
   public function removeArticle(Article $article ){
       $this->articles->removeElement($article);
       return $this;
   }

   /**
    * 
    * getArticles()
    */
   public function getArticles(){
       return $this->articles;
   }
   public function __toString() {
    return $this->pourcentageTva;
    }
}

