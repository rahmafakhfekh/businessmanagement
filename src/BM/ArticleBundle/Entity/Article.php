<?php

namespace BM\ArticleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use BM\ArticleBundle\Entity\Category;
use BM\ArticleBundle\Entity\Tva;
use BM\SalesBundle\Entity\ArticleDocument;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * ArticleBundle
 *
 * @ORM\Table(name="article")
 * @ORM\Entity(repositoryClass="BM\ArticleBundle\Repository\ArticleBundleRepository")
 */
class Article
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255)
     */
    private $designation;

    /**
     * @var string
     *
     * @ORM\Column(name="PriceVDHT", type="string", length=255)
     */
    private $priceVDHT;

    /**
     * @var string
     *
     * @ORM\Column(name="PriceVDTTC", type="string", length=255)
     */
    private $priceVDTTC;

    /**
     * @var string
     *
     * @ORM\Column(name="priceVGHT", type="string", length=255)
     */
    private $priceVGHT;
    /**
     * @var string
     *
     * @ORM\Column(name="PriceVGTTC", type="string", length=255)
     */
    private $priceVGTTC;

    /**
     * @var string
     *
     * @ORM\Column(name="PriceAHT", type="string", length=255)
     */
    private $priceAHT;

    /**
     * @var string
     *
     * @ORM\Column(name="PriceATTC", type="string", length=255)
     */
    private $priceATTC;

    /**
     * @var bool
     *
     * @ORM\Column(name="Status", type="boolean")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="Cut", type="string", length=255,nullable=true)
     */
    private $cut;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255,nullable=true)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="height", type="string", length=255,nullable=true)
     */
    private $height;

    /**
     * @var string
     *
     * @ORM\Column(name="width", type="string", length=255,nullable=true)
     */
    private $width;

    /**
     * @var string
     *
     * @ORM\Column(name="length", type="string", length=255,nullable=true)
     */
    private $length;

    /**
     * @var string
     *
     * @ORM\Column(name="weight", type="string", length=255,nullable=true)
     */
    private $weight;

    /**
    * @ORM\ManyToOne(targetEntity="BM\ArticleBundle\Entity\Category" , inversedBy="articles")
    * @ORM\JoinColumn(name="category_id", referencedColumnName="id",onDelete="SET NULL" )
    */
   private $category;
    /**
        * @ORM\ManyToOne(targetEntity="BM\ArticleBundle\Entity\tva" , inversedBy="articles")
        * @ORM\JoinColumn(name="tva_id", referencedColumnName="id",onDelete="SET NULL")
        */

    private $tva ;
    /**
     * @var string
     *
     * @ORM\Column(name="barCode", type="string", length=255)
     */
    private $barCode;

     /**
     * @var string
     *
     * @ORM\Column(name="qt", type="string", length=255)
     */
    private $qt;
    /**
    * @ORM\OneToOne(targetEntity="BM\ArticleBundle\Entity\Stock" , mappedBy="article")
    */
    private $stock;

    /**
    * @ORM\OneToMany(targetEntity="BM\SalesBundle\Entity\ArticleDocument", mappedBy="article")
    */
   private $articleDocuments;
   public function __construct(){
       $this-> articleDocuments = new ArrayCollection();
       
   }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return ArticleBundle
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return ArticleBundle
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set priceVDHT
     *
     * @param string $priceVDHT
     *
     * @return ArticleBundle
     */
    public function setPriceVDHT($priceVDHT)
    {
        $this->priceVDHT = $priceVDHT;

        return $this;
    }

    /**
     * Get priceVDHT
     *
     * @return string
     */
    public function getPriceVDHT()
    {
        return $this->priceVDHT;
    }

    /**
     * Set priceVDTTC
     *
     * @param string $priceVDTTC
     *
     * @return ArticleBundle
     */
    public function setPriceVDTTC($priceVDTTC)
    {
        $this->priceVDTTC = $priceVDTTC;

        return $this;
    }

    /**
     * Get priceVDTTC
     *
     * @return string
     */
    public function getPriceVDTTC()
    {
        return $this->priceVDTTC;
    }
    /**
     * Set priceVGHT
     *
     * @param string $priceVGHT
     *
     * @return ArticleBundle
     */
    public function setPriceVGHT($priceVGHT)
    {
        $this->priceVGHT = $priceVGHT;

        return $this;
    }

    /**
     * Get priceVGHT
     *
     * @return string
     */
    public function getPriceVGHT()
    {
        return $this->priceVGHT;
    }
    /**
     * Set priceVGTTC
     *
     * @param string $priceVGTTC
     *
     * @return ArticleBundle
     */
    public function setPriceVGTTC($priceVGTTC)
    {
        $this->priceVGTTC = $priceVGTTC;

        return $this;
    }

    /**
     * Get priceVGTTC
     *
     * @return string
     */
    public function getPriceVGTTC()
    {
        return $this->priceVGTTC;
    }

    /**
     * Set priceAHT
     *
     * @param string $priceAHT
     *
     * @return ArticleBundle
     */
    public function setPriceAHT($priceAHT)
    {
        $this->priceAHT = $priceAHT;

        return $this;
    }

    /**
     * Get priceAHT
     *
     * @return string
     */
    public function getPriceAHT()
    {
        return $this->priceAHT;
    }

    /**
     * Set priceATTC
     *
     * @param string $priceATTC
     *
     * @return ArticleBundle
     */
    public function setPriceATTC($priceATTC)
    {
        $this->priceATTC = $priceATTC;

        return $this;
    }

    /**
     * Get priceATTC
     *
     * @return string
     */
    public function getPriceATTC()
    {
        return $this->priceATTC;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return ArticleBundle
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set cut
     *
     * @param string $cut
     *
     * @return ArticleBundle
     */
    public function setCut($cut)
    {
        $this->cut = $cut;

        return $this;
    }

    /**
     * Get cut
     *
     * @return string
     */
    public function getCut()
    {
        return $this->cut;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return ArticleBundle
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set height
     *
     * @param string $height
     *
     * @return ArticleBundle
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return string
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set width
     *
     * @param string $width
     *
     * @return ArticleBundle
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return string
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set length
     *
     * @param string $length
     *
     * @return ArticleBundle
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length
     *
     * @return string
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set weight
     *
     * @param string $weight
     *
     * @return ArticleBundle
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return string
     */
    public function getWeight()
    {
        return $this->weight;
    }
    /**
     * Set category
     *
     * @param Category $category
     *
     * @return ArticleBundle
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Get tva
     *
     * @return string
     */
    public function getTva()
    {
        return $this->tva;
    }
      /**
     * Set tva
     *
     * @param Tva $tva
     *
     * @return ArticleBundle
     */
    public function setTva(Tva $tva)
    {
        $this->tva = $tva;

        return $this;
    }
    /**
     * Set barCode
     *
     * @param string $barCode
     *
     * @return ArticleBundle
     */
    public function setBarCode($barCode)
    {
        $this->barCode = $barCode;

        return $this;
    }

    /**
     * Get barCode
     *
     * @return string
     */
    public function getBarCode()
    {
        return $this->barCode;
    }




      /**
     * Set qt
     *
     * @param string $qt
     *
     * @return ArticleBundle
     */
    public function setQt($qt)
    {
        $this->qt = $qt;

        return $this;
    }

    /**
     * Get barCode
     *
     * @return string
     */
    public function getQt()
    {
        return $this->qt;
    }
    /**
     * Get stock
     *
     * @return string
     */
    public function getStock()
    {
        return $this->stock;
    }
      /**
     * Set stock
     *
     * @param Stock $stock
     *
     * @return ArticleBundle
     */
    public function setStock(Stock $stock)
    {
        $this->stock = $stock;

        return $this;
    }






  /**
    * 
    * addArticledocus()
    */
   public function addArticleDocument(ArticleDocument $articleDocument ){
       $this->articleDocuments->add($articleDocument);
       return $this;
   }
   /**
    * 
    * removeArticledocus()
    */
   public function removeArticleDocument(ArticleDocument $articleDocument ){
       $this->articleDocuments->removeElement($articleDocument);
       return $this;
   }

   /**
    * 
    * getArticledocus()
    */
   public function getArticleDocuments(){
       return $this->articleDocuments;
   }
   public function __toString() {
    return $this->designation;
    }
}

