<?php

namespace BM\ArticleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use BM\ArticleBundle\Entity\Article;
/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="BM\ArticleBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
    * @ORM\OneToMany(targetEntity="BM\ArticleBundle\Entity\Article", mappedBy="category")
    */
   private $articles;

   public function __construct(){
       $this-> articles = new ArrayCollection();
       
   }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
    * 
    * addArticle()
    */
   public function addArticle(Article $article ){
       $this->articles->add($article);
       return $this;
   }
   /**
    * 
    * removeArticle()
    */
   public function removeArticle(Article $article ){
       $this->articles->removeElement($article);
       return $this;
   }

   /**
    * 
    * getArticles()
    */
   public function getArticles(){
       return $this->articles;
   }
   public function __toString() {
    return $this->name;
    }
}

