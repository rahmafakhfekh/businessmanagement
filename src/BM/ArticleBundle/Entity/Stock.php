<?php

namespace BM\ArticleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use BM\ArticleBundle\Entity\Article;


/**
 * Stock
 *
 * @ORM\Table(name="stock")
 * @ORM\Entity(repositoryClass="BM\ArticleBundle\Repository\stockRepository")
 */
class Stock
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
    * @ORM\OneToOne(targetEntity="BM\ArticleBundle\Entity\Article" , inversedBy="stock")
    */
   private $article;

    /**
     * @var string
     *
     * @ORM\Column(name="QT", type="string", length=255)
     */
    private $qT;





    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qT
     *
     * @param string $qT
     *
     * @return stock
     */
    public function setQT($qT)
    {
        $this->qT = $qT;

        return $this;
    }

    /**
     * Get qT
     *
     * @return string
     */
    public function getQT()
    {
        return $this->qT;
    }


     /**
     * Set article
     *
     * @param string $article
     *
     * @return stock
     */
    public function setArticle(Article $article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return string
     */
    public function getArticle()
    {
        return $this->article;
    }
}

