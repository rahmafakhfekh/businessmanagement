<?php

namespace BM\ProviderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProviderType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nomFour')->add('raisonSocialFour')->add('codPosFour', NumberType::class, array(
            'required' => false,
            'attr' => array(
                                
                               ),
            ))
        ->add('villeFour')->add('paysFour', TextType::class, array(
            'required' => false,
            'attr' => array(
                                
                               ),
            ))->add('telFour', NumberType::class, array(
            'required' => true,
            'attr' => array(
                                
                               ),
            ))->add('emailFour')->add('commantaireFour', TextType::class, array(
            'required' => false,
            'attr' => array(
                                
                               ),
            ))->add('faxFour', TextType::class, array(
            'required' => false,
            'attr' => array(
                                
                               ),
            ))
            ->add('siteWebFour', TextType::class, array(
            'required' => false,
            'attr' => array(
                                
                               ),
            ))->add('matriculeFiscalFour')->add('codDouaneFour')->add('codeFour');
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BM\ProviderBundle\Entity\Provider'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'bm_providerbundle_provider';
    }


}
