<?php

namespace BM\ProviderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Provider
 *
 * @ORM\Table(name="provider")
 * @ORM\Entity(repositoryClass="BM\ProviderBundle\Repository\ProviderRepository")
 */
class Provider
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


     /**
     * @var string
     *
     * @ORM\Column(name="codeFour", type="string", length=255)
     */
    private $codeFour;

    /**
     * @var string
     *
     * @ORM\Column(name="nomFour", type="string", length=255 ,nullable=true)
     */
    private $nomFour;

    /**
     * @var string
     *
     * @ORM\Column(name="raisonSocialFour", type="string", length=255 ,nullable=true)
     */
    private $raisonSocialFour;

    /**
     * @var string
     *
     * @ORM\Column(name="codPosFour", type="string", length=255 ,nullable=true)
     */
    private $codPosFour;

    /**
     * @var string
     *
     * @ORM\Column(name="villeFour", type="string", length=255 ,nullable=true)
     */
    private $villeFour;

    /**
     * @var string
     *
     * @ORM\Column(name="paysFour", type="string", length=255 ,nullable=true)
     */
    private $paysFour;

    /**
     * @var string
     *
     * @ORM\Column(name="telFour", type="string", length=255 ,nullable=true)
     */
    private $telFour;

    /**
     * @var string
     *
     * @ORM\Column(name="emailFour", type="string", length=255 ,nullable=true)
     */
    private $emailFour;

    /**
     * @var string
     *
     * @ORM\Column(name="commantaireFour", type="string", length=255 ,nullable=true)
     */
    private $commantaireFour;

    /**
     * @var string
     *
     * @ORM\Column(name="faxFour", type="string", length=255 ,nullable=true)
     */
    private $faxFour;

    /**
     * @var string
     *
     * @ORM\Column(name="siteWebFour", type="string", length=255 ,nullable=true)
     */
    private $siteWebFour;

    /**
     * @var string
     *
     * @ORM\Column(name="matriculeFiscalFour", type="string", length=255 ,nullable=true)
     */
    private $matriculeFiscalFour;

    /**
     * @var string
     *
     * @ORM\Column(name="codDouaneFour", type="string", length=255 ,nullable=true)
     */
    private $codDouaneFour;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
 /**
     * Set codeFour
     *
     * @param string $codeFour
     *
     * @return fournisseurs1
     */
    public function setcodeFour($codeFour)
    {
        $this->codeFour = $codeFour;

        return $this;
    }

/**
     * Get codeFour
     *
     * @return string
     */
    public function getCodeFour()
    {
        return $this->codeFour;
    }




    /**
     * Set nomFour
     *
     * @param string $nomFour
     *
     * @return fournisseurs1
     */
    public function setNomFour($nomFour)
    {
        $this->nomFour = $nomFour;

        return $this;
    }

    /**
     * Get nomFour
     *
     * @return string
     */
    public function getNomFour()
    {
        return $this->nomFour;
    }

    /**
     * Set raisonSocialFour
     *
     * @param string $raisonSocialFour
     *
     * @return fournisseurs1
     */
    public function setRaisonSocialFour($raisonSocialFour)
    {
        $this->raisonSocialFour = $raisonSocialFour;

        return $this;
    }

    /**
     * Get raisonSocialFour
     *
     * @return string
     */
    public function getRaisonSocialFour()
    {
        return $this->raisonSocialFour;
    }

    /**
     * Set codPosFour
     *
     * @param string $codPosFour
     *
     * @return fournisseurs1
     */
    public function setCodPosFour($codPosFour)
    {
        $this->codPosFour = $codPosFour;

        return $this;
    }

    /**
     * Get codPosFour
     *
     * @return string
     */
    public function getCodPosFour()
    {
        return $this->codPosFour;
    }

    /**
     * Set villeFour
     *
     * @param string $villeFour
     *
     * @return fournisseurs1
     */
    public function setVilleFour($villeFour)
    {
        $this->villeFour = $villeFour;

        return $this;
    }

    /**
     * Get villeFour
     *
     * @return string
     */
    public function getVilleFour()
    {
        return $this->villeFour;
    }

    /**
     * Set paysFour
     *
     * @param string $paysFour
     *
     * @return fournisseurs1
     */
    public function setPaysFour($paysFour)
    {
        $this->paysFour = $paysFour;

        return $this;
    }

    /**
     * Get paysFour
     *
     * @return string
     */
    public function getPaysFour()
    {
        return $this->paysFour;
    }

    /**
     * Set telFour
     *
     * @param string $telFour
     *
     * @return fournisseurs1
     */
    public function setTelFour($telFour)
    {
        $this->telFour = $telFour;

        return $this;
    }

    /**
     * Get telFour
     *
     * @return string
     */
    public function getTelFour()
    {
        return $this->telFour;
    }

    /**
     * Set emailFour
     *
     * @param string $emailFour
     *
     * @return fournisseurs1
     */
    public function setEmailFour($emailFour)
    {
        $this->emailFour = $emailFour;

        return $this;
    }

    /**
     * Get emailFour
     *
     * @return string
     */
    public function getEmailFour()
    {
        return $this->emailFour;
    }

    /**
     * Set commantaireFour
     *
     * @param string $commantaireFour
     *
     * @return fournisseurs1
     */
    public function setCommantaireFour($commantaireFour)
    {
        $this->commantaireFour = $commantaireFour;

        return $this;
    }

    /**
     * Get commantaireFour
     *
     * @return string
     */
    public function getCommantaireFour()
    {
        return $this->commantaireFour;
    }

    /**
     * Set faxFour
     *
     * @param string $faxFour
     *
     * @return fournisseurs1
     */
    public function setFaxFour($faxFour)
    {
        $this->faxFour = $faxFour;

        return $this;
    }

    /**
     * Get faxFour
     *
     * @return string
     */
    public function getFaxFour()
    {
        return $this->faxFour;
    }

    /**
     * Set siteWebFour
     *
     * @param string $siteWebFour
     *
     * @return fournisseurs1
     */
    public function setSiteWebFour($siteWebFour)
    {
        $this->siteWebFour = $siteWebFour;

        return $this;
    }

    /**
     * Get siteWebFour
     *
     * @return string
     */
    public function getSiteWebFour()
    {
        return $this->siteWebFour;
    }

    /**
     * Set matriculeFiscalFour
     *
     * @param string $matriculeFiscalFour
     *
     * @return fournisseurs1
     */
    public function setMatriculeFiscalFour($matriculeFiscalFour)
    {
        $this->matriculeFiscalFour = $matriculeFiscalFour;

        return $this;
    }

    /**
     * Get matriculeFiscalFour
     *
     * @return string
     */
    public function getMatriculeFiscalFour()
    {
        return $this->matriculeFiscalFour;
    }

    /**
     * Set codDouaneFour
     *
     * @param string $codDouaneFour
     *
     * @return fournisseurs1
     */
    public function setCodDouaneFour($codDouaneFour)
    {
        $this->codDouaneFour = $codDouaneFour;

        return $this;
    }

    /**
     * Get codDouaneFour
     *
     * @return string
     */
    public function getCodDouaneFour()
    {
        return $this->codDouaneFour;
    }
}