<?php

namespace BM\ProviderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BMProviderBundle:Default:index.html.twig');
    }
}
