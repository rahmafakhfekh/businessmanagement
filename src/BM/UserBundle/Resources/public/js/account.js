$(document).ready(function(){
	$(document).on('click','.edituser.edit.lightbox',function() {
		var id = $(this).attr('data-id');
		url= Routing.generate('backend_account_editUser', { _locale: locale, id: id  });
		loadContentPopup(url,'edituser','Campaign_name');

	});
	//delete user with confirmation
	$(document).on('click','.deleteuser.delet.lightbox', function() {
		var id = $(this).attr('data-id');
		var type = $(this).attr('data-type');
		url= Routing.generate('backend_account_delete', { _locale: locale, id: id ,type:type });
		$("#deleteuser .categ-block").load(url, function(){});
		  
	});
	$(document).on('click','.adduserByCompany.lightbox',function() {
		var idCompany = $(this).attr('data-id');
		url= Routing.generate('backend_account_createUserByCompany', { _locale: locale, idCompany: idCompany  });
		loadContentPopup(url,'adduserByCompany','Campaign_name');

	});
	$(document).on('click','.edituserByCompany.lightbox',function() {
		var idCompany = $(this).attr('data-id-company');
		var id = $(this).attr('data-id');
		url= Routing.generate('backend_account_editUserByCompany', { _locale: locale, id: id, idCompany: idCompany  });
		loadContentPopup(url,'edituserByCompany','Campaign_name');

	});
	$(document).on('click','.addsms.lightbox',function() {
		var idCompany = $(this).attr('data-id');
		url= Routing.generate('backend_sms_createSms', { _locale: locale, idCompany: idCompany  });
		loadContentPopup(url,'addsms','Sms');

	});
	$(document).on('click','.editsms.lightbox',function() {
		
		var id = $(this).attr('data-id');
		url= Routing.generate('backend_sms_edit', { _locale: locale, id: id  });
		loadContentPopup(url,'editsms','Sms');

	});
	//delete user with confirmation
	$(document).on('click','.deletesms.delet.lightbox', function() {
		var id = $(this).attr('data-id');
		url= Routing.generate('backend_sms_delete', { _locale: locale, id: id });
		$("#deletesms .categ-block").load(url, function(){});
		  
	});
	//submit account settings form

    $(document).on('click','.save', function(event){
	    //event.preventDefault();
	 	var id = $('#id_user').val();
	  
		//validate email login , it should be unique
		var email  = $('#Account_email').val();
	    var valid_user_email = validate_user_email(email,id);	
	    if (valid_user_email) {
		    document.getElementById("Account_email").setCustomValidity(usedEmail);
		} else {
		    document.getElementById("Account_email").setCustomValidity('');
	    }
	    var password = $('#Account_currentPassword').val();

		var new_password = $('#Account_newPassword').val();
		var new_password2 = $('#Account_newRetypedPassword').val();
		// var first_login  = $('#login_user').val();
		var first_email  = $('#email_user').val();
		if (password != '')
			var valid_password = validate_user_password(password,id);
		
	    //type current password if he want to change his email
	    if (!valid_user_email && first_email != email && password == '') {
		    document.getElementById("Account_currentPassword").setCustomValidity(typeCurrentPassEmail);
		} else if (!valid_user_email && first_email != email && password != '' && valid_password) {
		    document.getElementById("Account_currentPassword").setCustomValidity('');
	    }
	    //verify current password (fault password) if he want to change his email
	 	if (!valid_user_email && first_email != email && password != '' && !valid_password) {
		    document.getElementById("Account_currentPassword").setCustomValidity(verifyCurrentPassEmail);
		} else if (!valid_user_email && first_email != email && password != '' && valid_password) {
		    document.getElementById("Account_currentPassword").setCustomValidity('');
	    }
	    //type current password in order to change the password (current =='')
	    if ((password == '' && new_password == '' && new_password2 != '') || (password == '' && new_password != '' && new_password2 == '')|| (password == '' && new_password != '' && new_password2 != '')) {
		    document.getElementById("Account_currentPassword").setCustomValidity(typeCurrentPassPass);
		} else if (password == '' && new_password == '' && new_password2 == '' && !valid_user_email && first_email == email ) {
		    document.getElementById("Account_currentPassword").setCustomValidity('');
	    }
	    //verify current password in order to change the password (current is fault)
	    if ((password != '' && new_password == '' && new_password2 != '' && !valid_password) || (password != '' && new_password != '' && new_password2 == '' && !valid_password)) {
		    document.getElementById("Account_currentPassword").setCustomValidity(verifyCurrentPassPass);
		} else if (password != '' && valid_password){
		    document.getElementById("Account_currentPassword").setCustomValidity('');
	    }
	    //verify current password in order to change the password
	    if ((password != '' && new_password != new_password2 && !valid_password) || (password == '' && new_password == new_password2 && new_password != '')) {
		    document.getElementById("Account_currentPassword").setCustomValidity(verifyCurrentPassPass);
		} else if (password != '' && new_password == new_password2 && valid_password){
		    document.getElementById("Account_currentPassword").setCustomValidity('');
	    }
	    if ((password != '' && new_password != new_password2 && valid_password)) {
		    document.getElementById("Account_newPassword").setCustomValidity(twoPass);
		} else if (password != '' && new_password == new_password2 && valid_password){
		    document.getElementById("Account_newPassword").setCustomValidity('');
	    }
	    if (password != '' && new_password == '' && new_password2 == '' && !valid_password) {
			document.getElementById("Account_currentPassword").setCustomValidity(verifyCurrentPass);
		} else if (password != '' && new_password == '' && new_password2 == '' && valid_password){
		    document.getElementById("Account_currentPassword").setCustomValidity('');
	    }
	   // return false;
	    
	    //validate phone number
	    var pattern_landline = new RegExp(/^\+90\s((?!(50[1567])|(53\d)|(54\d)|(55[123459])|(59[4]))\d{3})\s\d{7}$/);
	    var pattern_mobile = new RegExp(/^\+90\s\d{3}\s\d{7}$/);
	    var phone = $('#Account_phoneNumber').val();
	    //accept mobile and landline number for account settings
	    /*if (!pattern_landline.test(phone)) {
	    	document.getElementById("Account_phoneNumber").setCustomValidity(landlinePhone);
		} else {
		    document.getElementById("Account_phoneNumber").setCustomValidity('');
	    }*/
		
	   
    });
    
    function validate_user_email($email,$id) {

		url= Routing.generate('backend_account_validate_email', { _locale:'de', email: $email, id: $id  });
  
		var exist = $.ajax({
                    type: "POST",
                    url: url,
                    data: $(this).serialize(),
                    dataType: "json",
                    async: false
                       }).responseText;

	    var dataArray = jQuery.parseJSON(exist);
	    return dataArray.exist;
    }
    function validate_user_password($password,$id) {

		url= Routing.generate('backend_account_validate_password', {_locale:'de', password: $password, id: $id  });
  
		var match = $.ajax({
                    type: "POST",
                    url: url,
                    data: $(this).serialize(),
                    dataType: "json",
                    async: false
                       }).responseText;

	    var dataArray = jQuery.parseJSON(match);
	    return dataArray.match;
    }

    //sort columns
	$("#myTable").tablesorter(); 
	$("#myTable1").tablesorter(); 
});
function loadContentPopup(url,idPopup,idInput) {
	$("#"+idPopup+" .categ-block").load(url, function(){
			
		jcf.customForms.replaceAll();
		$('#'+idPopup+' .categ-block input, #'+idPopup+' .categ-block textarea').each(function() {
	    	$(this).addClass('text-active');
			$(this).parent().addClass('parent-active');
			$(this).focusout(function() {

				$(this).removeClass('focus');
				$(this).parent().removeClass('parent-focus');
			});
	    	$(this).focus(function() {
	    		
	    		$(this).addClass('focus text-active');
	    		$(this).parent().addClass('parent-focus parent-active');
			 
			});
	    })
	    var text = $('#'+idPopup+' .categ-block input#'+idInput+'').val();
		var input = $('#'+idPopup+' .categ-block input#'+idInput+'').closest('.row');
		var maxLenght = $('#'+idPopup+' .categ-block input#'+idInput+'').attr('maxlength');
		var length = text.length;
  		input.find('span').html(maxLenght - length);
		if (idPopup == 'editmedium' || idPopup == 'addmedium') {
			$(document).on('change','#Medium_number',function(){
				if ($(this).val() != '')
				$("#"+idPopup+" .categ-block .popup-target-number").html($('li#'+$(this).val()).html());
				else
					$("#"+idPopup+" .categ-block .popup-target-number").html(' ');
			})
		}		
	});
}