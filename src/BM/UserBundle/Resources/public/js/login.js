$(document).ready(function(){
	$('#forgetPasswordSubmit').on('click',function(event){
		//event.preventDefault();
    	var email  = $('#emailForgetPassword').val();
    	
		if (email != '')
	    var valid_user_email = validate_user_email(email);	   
	   
	    if (!valid_user_email) {
		    document.getElementById("emailForgetPassword").setCustomValidity(noEmail);
		} else {
		    document.getElementById("emailForgetPassword").setCustomValidity('');
	    }
	    //return false;
    });
	$(document).on('click','.cancel',function() {
   	    parent.$.fancybox.close();
   	    return false;
  	}); 
  			
	$('#forgetPassword').submit(function(e){
		e.preventDefault(); 
		
		var url = $(this).attr('action');
	    $form = $(this);
	    var $loading = $form.find('.loading-signup').hide();
	    $.ajax({
		    url:url,
		    type:'POST',
		    data: $(this).serialize(),
	        dataType: "html",
	        beforeSend: function(){
				$loading.show();
	        },
	        success: function(data){
        		
        		$('div.categ-block').hide();
				$('.message_registration').html("<p style=\"text-align: center;\">"+resetPassMsg1+"<br/>"+ 
				resetPassMsg2+"</p><div class=\"btn-holder\" style=\"text-align: center;\">"+
				"<button class=\"save close_popop\" style=\"float: none;\"><strong> OK</strong></button>"+
				"</div><br/>");
			
	        },
	        error:function(jqXHR , status, error){
				$form.find('.error:first').html('error processing request ');
				$loading.hide();
				console.log(error);
	        }
	   });
	
	});
	$(document).on('click','.close_popop',function() {
		location.reload();
	})
});
function validate_user_email($email) {

		url= Routing.generate('backend_account_validate_email_withoutId', { email: $email });
		var exist = $.ajax({
                    type: "POST",
                    url: url,
                    data: $(this).serialize(),
                    dataType: "json",
                    async: false
                       }).responseText;
	    var dataArray = jQuery.parseJSON(exist);
	    return dataArray.exist;
   
}