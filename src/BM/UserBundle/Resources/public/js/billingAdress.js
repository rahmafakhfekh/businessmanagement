$(document).ready(function(){
	// setting placeholder attributes
	// $('#BillingAdress_firstName').attr('placeholder','First Name');
	// $('#BillingAdress_lastName').attr('placeholder','Last Name');
	// $('#BillingAdress_companyName').attr('placeholder','Company Name');
	// $('#BillingAdress_legalForm').attr('placeholder','Legal Form');
	// $('#BillingAdress_street').attr('placeholder','Street');
	// $('#BillingAdress_number').attr('placeholder','Number');
	// $('#BillingAdress_additive').attr('placeholder','Additive');
	// $('#BillingAdress_postCode').attr('placeholder','Postcode');
	// $('#BillingAdress_taxNumber').attr('placeholder','Tax Number');
	// $('#BillingAdress_taxOffice').attr('placeholder','Tax Office');

	// $('.first_name').attr('placeholder','First Name');
	// $('.last_name').attr('placeholder','Last Name');
	// $('.phone_number').attr('placeholder','Telephone');
	// $('.email_adress').attr('placeholder','E-mail');
	//contact required when change to another
	$(document).on('click','#BillingAdress_contactMe_1', function() {
		$('.first_name').attr('required','required');
		$('.last_name').attr('required','required');
		$('.phone_number').attr('required','required');
		$('.email_adress').attr('required','required');
	});
	$(document).on('click','#BillingAdress_contactMe_0', function() {
		$('.first_name').attr('required',false);
		$('.last_name').attr('required',false);
		$('.phone_number').attr('required',false);
		$('.email_adress').attr('required',false);
	});
	
	// firing chang event on tax input to update label
	setTimeout(function(){
		$('#sertificaa_accountbundle_billingadress_idNumber').attr('placeholder',taxId);
	}, 50);
	
	
	
	// disabling tabs ( we cant open them by click) , we have to go step by step 
	var tabs= ['#billing_data','#payement','#confirmation']
	for (index in tabs ) {
	$('a[href="'+tabs[index]+'"]').bind('click', disableLink);
	};

	$('.continue').on('click',function(){
		// enabling next tab 
		$('a[href="#'+$(this).attr('data-next')+'"]').unbind('click',disableLink);
		$('#upgradeTab a[href="#'+$(this).attr('data-next')+'"]').tab('show');
	});

	


	$('#goToPayement').on('click', function(){
		console.log('Next is clicked !!');
		$('#newBillingAdress').trigger('submit');
	});
	if ($('input[name="BillingAdress[contactMe]"]:checked').val() == 'me') {
			$('.me').show();
			$('.another').hide();
			$('.contact').html('').removeAttr('required');
		}
		else {
			$('.me').hide();
			$('.another').show();
			$('.contact').attr('required','required')
			
		}
	// update contact widget 
	$('input[name="BillingAdress[contactMe]"]').on('change',function(){
		// current div to show is held in current value of radio button
		// the other view to show is held in data-other oattribute of current (clicked) radio button 
		var currentDivId = $(this).val() ;
		console.log(currentDivId);
		if (currentDivId == 'me') {
			$('.me').show();
			$('.another').hide();
			$('.contact').html('').removeAttr('required');
		}
		else {
			$('.me').hide();
			$('.another').show();
			$('.contact').attr('required','required')
			
		}
		
	})
	// dynamicaly changing tax field if real person is checked 
	// getting initial value 
	var initialValue = $('input[name="BillingAdress[legalPerson]"]:checked').val() ;
	var initialInputValue = $('input[data-legal="taxIdOrNumber"]').val() ;

	$('input[name="BillingAdress[legalPerson]"]').change(function(){
		var $input = $('input[data-legal="taxIdOrNumber"]') ;
		if ($(this).val() !== initialValue ){
			$input.val('');
		} else {
			$input.val(initialInputValue);
		}
		if ($(this).val() === 'yes'){
			$input.attr('name', 'BillingAdress[taxNumber]');
			$input.attr('placeholder',taxNumber);
			jcf.customForms.replaceAll();

		} else {
			$input.attr('name', 'BillingAdress[idNumber]');
			$input.attr('placeholder',taxId);
			jcf.customForms.replaceAll();
		}
	});

// $('.tooltipTel').tooltipster({
 //   		position: 'right'
	// });
	
	//add format design to phone field with jquery.inputmask.js
	// $.extend($.inputmask.defaults.definitions, {
	    
	//      'z': {
	//         validator: function (chrs, buffer, pos, strict, opts) {
	//                 var valExp2 = new RegExp(/((?!(50[1567])|(53\d)|(54\d)|(55[123459])|(59[4]))\d{3})/);
	                
	//         		if (!valExp2.test(chrs)) {
	//         			$('#sertificaa_accountbundle_billingadress_phoneNumber').parent().tooltipster('show');
	//         			//$('.save').trigger('click');
	// 				} 
	// 				else {
	//         			$('#sertificaa_accountbundle_billingadress_phoneNumber').parent().tooltipster('hide');

	// 			    }

	//                  return valExp2.test( chrs);
	//         },
	//         cardinality:3,
	//         definitionSymbol: "i"
	//      },
	     
	// });
	// $("#sertificaa_accountbundle_billingadress_phoneNumber").
	// 	inputmask("+\\90 z 9999999");
	// setting placeholder attributes
	// $('#sertificaa_accountbundle_billingadress_companyName').attr('placeholder','The legal name of Company');
	// $('#sertificaa_accountbundle_billingadress_adress').attr('placeholder','Please enter your adress');
	// $('#sertificaa_accountbundle_billingadress_idNumber').attr('placeholder','Tax Number');
	// $('#sertificaa_accountbundle_billingadress_taxOffice').attr('placeholder','Tax Office');
	// $('.first_name').attr('placeholder','First Name');
	// $('.last_name').attr('placeholder','Last Name');
	// $('.phone_number').attr('placeholder','Telephone');
	// $('.email_adress').attr('placeholder','E-mail');	
});


function disableLink(e){
	e.preventDefault();
	return false ;
}





