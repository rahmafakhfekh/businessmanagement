$(document).ready(function(){

	//delete user with confirmation
	$(document).on('click','.deleteuser.delet.lightbox', function() {
		var id = $(this).attr('data-id');
		var type = $(this).attr('data-type');
		url= Routing.generate('backend_account_delete', { _locale: locale, id: id ,type:type });
		$("#deleteuser .categ-block").load(url, function(){});
		  
	});
	$(document).on('click','.adduserByCompany.lightbox',function() {
		var idCompany = $(this).attr('data-id');
		url= Routing.generate('backend_account_createUserByCompany', { _locale: locale, idCompany: idCompany  });
		loadContentPopup(url,'adduserByCompany','Campaign_name');

	});
	$(document).on('click','.edituserByCompany.lightbox',function() {
		var idCompany = $(this).attr('data-id-company');
		var id = $(this).attr('data-id');
		url= Routing.generate('backend_account_editUserByCompany', { _locale: locale, id: id, idCompany: idCompany  });
		loadContentPopup(url,'edituserByCompany','Campaign_name');

	});
	$(document).on('click','.addsms.lightbox',function() {
		var idCompany = $(this).attr('data-id');
		url= Routing.generate('backend_sms_createSms', { _locale: locale, idCompany: idCompany  });
		loadContentPopup(url,'addsms','Sms');

	});
	$(document).on('click','.editsms.lightbox',function() {
		
		var id = $(this).attr('data-id');
		url= Routing.generate('backend_sms_edit', { _locale: locale, id: id  });
		loadContentPopup(url,'editsms','Sms');

	});
	//delete user with confirmation
	$(document).on('click','.deletesms.delet.lightbox', function() {
		var id = $(this).attr('data-id');
		url= Routing.generate('backend_sms_delete', { _locale: locale, id: id });
		$("#deletesms .categ-block").load(url, function(){});
		  
	});
	
	/*numbers */
	$(document).on('click','.addnumberByCompany.lightbox',function() {
		var idCompany = $(this).attr('data-id');
		url= Routing.generate('backend_number_createByCompany', { _locale: locale, idCompany: idCompany });
		loadContentPopup(url,'addnumberByCompany','Sms');

	});
	$(document).on('click','.editnumberByCompany.lightbox',function() {
		var id = $(this).attr('data-id');
		var idCompany = $(this).attr('data-id-company');
		url= Routing.generate('backend_number_editByCompany', { _locale: locale, id: id, idCompany: idCompany  });
		loadContentPopup(url,'editnumberByCompany','Sms');

	});
	//delete user with confirmation
	$(document).on('click','.deletenumberByCompany.delet.lightbox', function() {
		var id = $(this).attr('data-id');
		url= Routing.generate('backend_number_delete', { _locale: locale, id: id });
		$("#deletenumberByCompany .categ-block").load(url, function(){});
		  
	});

	/*mediums */
	$(document).on('click','.addmediumByCompany.lightbox',function() {
		var idCompany = $(this).attr('data-id');
		url= Routing.generate('backend_medium_createByCompany', { _locale: locale, idCompany: idCompany });
		loadContentPopup(url,'addmediumByCompany','Sms');

	});
	$(document).on('click','.editmediumByCompany.lightbox',function() {
		var id = $(this).attr('data-id');
		var idCompany = $(this).attr('data-id-company');
		url= Routing.generate('backend_medium_editByCompany', { _locale: locale, id: id, idCompany: idCompany  });
		loadContentPopup(url,'editmediumByCompany','Sms');

	});
	//delete user with confirmation
	$(document).on('click','.deletemediumByCompany.delet.lightbox', function() {
		var id = $(this).attr('data-id');
		url= Routing.generate('backend_medium_delete', { _locale: locale, id: id });
		$("#deletemediumByCompany .categ-block").load(url, function(){});
		  
	});
	

    //sort columns
	$("#myTable").tablesorter(); 
	$("#myTable1").tablesorter(); 
});
function loadContentPopup(url,idPopup,idInput) {
	$("#"+idPopup+" .categ-block").load(url, function(){
			
		jcf.customForms.replaceAll();
		$('#'+idPopup+' .categ-block input, #'+idPopup+' .categ-block textarea').each(function() {
	    	$(this).addClass('text-active');
			$(this).parent().addClass('parent-active');
			$(this).focusout(function() {

				$(this).removeClass('focus');
				$(this).parent().removeClass('parent-focus');
			});
	    	$(this).focus(function() {
	    		
	    		$(this).addClass('focus text-active');
	    		$(this).parent().addClass('parent-focus parent-active');
			 
			});
	    })
	    var text = $('#'+idPopup+' .categ-block input#'+idInput+'').val();
		var input = $('#'+idPopup+' .categ-block input#'+idInput+'').closest('.row');
		var maxLenght = $('#'+idPopup+' .categ-block input#'+idInput+'').attr('maxlength');
		//var length = text.length;
  		//input.find('span').html(maxLenght - length);
		if (idPopup == 'editmediumByCompany' || idPopup == 'addmediumByCompany') {
			$(document).on('change','#Medium_number',function(){
				if ($(this).val() != '')
				$("#"+idPopup+" .categ-block .popup-target-number").html($('li#'+$(this).val()).html());
				else
					$("#"+idPopup+" .categ-block .popup-target-number").html(' ');
			})
		}		
	});
}