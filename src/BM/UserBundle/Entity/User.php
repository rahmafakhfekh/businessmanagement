<?php

namespace BM\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\EquatableInterface ;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 * @ORM\Entity(repositoryClass="BM\UserBundle\Repository\UserRepository")
 * @ORM\Table()
 */
class User implements UserInterface, \Serializable 
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="ActivationToken", type="string", length=255)
     */
    private $activationToken;

    /**
     * @var string
     *
     * @ORM\Column(name="Email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="FirstName", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="LastName", type="string", length=255, nullable=true)
     */
    private $lastName;

    
    /**
     * @var string
     *
     * @ORM\Column(name="Password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="phoneNumber", type="string", length=15, nullable=true)
     */
    private $phoneNumber;

   
    

     /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     *
     */
    private $roles;

     /**
     * @var \DateTime
     *
     * @ORM\Column(name="subscriptionDate", type="datetime")
     */
    private $subscriptionDate;
   /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     */
    private $username;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
   
    

    public function __construct()
    {
        
        $this->roles = new ArrayCollection();

    }

   

    /**
     * Set activationToken
     *
     * @param string $activationToken
     * @return User
     */
    public function setActivationToken($activationToken)
    {
        $this->activationToken = $activationToken;
    
        return $this;
    }

    /**
     * Get activationToken
     *
     * @return string 
     */
    public function getActivationToken()
    {
        return $this->activationToken;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    
        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    
    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
       return $this->roles->toArray();
    }
/**
     * Returns the salt that was originally used to encode the password.
     *
     * {@inheritdoc}
     */
    public function getSalt()
    {
        // See "Do you need to use a Salt?" at https://symfony.com/doc/current/cookbook/security/entity_provider.html
        // we're using bcrypt in security.yml to encode the password, so
        // the salt value is built-in and you don't have to generate one
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }
    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
        ) = unserialize($serialized);
    }

    
    

    
    /**
     * Add roles
     *
     * @param \BM\UserBundle\Entity\Role $roles
     * @return User
     */
    public function addRole(\BM\UserBundle\Entity\Role $roles)
    {
        $this->roles[] = $roles;
    
        return $this;
    }

    /**
     * Remove roles
     *
     * @param \BM\UserBundle\Entity\Role $roles
     */
    public function removeRole(\BM\UserBundle\Entity\Role $roles)
    {
        $this->roles->removeElement($roles);
    }

    
    /**
     * Get roles attribute since there is a method getRoles required by UserInterface
     * we are using this method instead
     * @return $roles
     */
    public function getRolesAttribute()
    {
        return $this->roles;
    }
    /**
     * set roles Attribute 
     * @param [type] $roles [description]
     */
    public function setRolesAttribute($roles){
        $this->roles = $roles ;
        
        return $this;
    }

    

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     * @return AccountBasicInfo
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    
        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string 
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    
    /**
     * Set subscriptionDate
     *
     * @param \DateTime $subscriptionDate
     * @return AccountBasicInfo
     */
    public function setSubscriptionDate($subscriptionDate)
    {
        $this->subscriptionDate = $subscriptionDate;
    
        return $this;
    }

    /**
     * Get subscriptionDate
     *
     * @return \DateTime 
     */
    public function getSubscriptionDate()
    {
        return $this->subscriptionDate;
    }

    

   
}