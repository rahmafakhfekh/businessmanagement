<?php
namespace Track\AccountBundle\Services\Backend;

use Symfony\Component\Security\Core\Util\StringUtils;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
class AccountServices {

	private $em ;
    private $securityContext;
    private $container;
	public function __construct($em, $securityContext, $container){
        $this->em = $em;
        $this->securityContext = $securityContext;
        $this->container = $container;
	}
    public function newEntityAccount(){
        $entity = $this->container->get('account.factory')->createOne();
        return $entity ;
    }
	public function editAccountForm($id){
        $entity = $this->em->getRepository('TrackAccountBundle:Account')->find( $id);
        
        return $entity ;
	}
    public function getEntityAccount($id){
        $entity = $this->em->getRepository('TrackAccountBundle:Account')->find( $id);
        
        return $entity ;
    }
    public function editAccountSubmit($id, $currentPassword, $newPassword,$actualPassword) {
           
        $entity = $this->em->getRepository('TrackAccountBundle:Account')->find( $id);
        $encoderFactory = $this->container->get('security.encoder_factory');
        

        $encoder = $encoderFactory->getEncoder($entity);
        //$actualPassword = $entity->getPassword();
        $salt = $entity->getSalt();
        $passwordsMatch =  $this->matchPasswords( $currentPassword , $actualPassword, $encoder , $salt);
        if ($newPassword != '') {
            $newEncodedPassword = $this->encodePassword($newPassword, $encoder , $salt);
            $entity->setPassword($newEncodedPassword);
        }
        else {
            $entity->setPassword($actualPassword);
        }
        
        $this->em->persist($entity);
        $this->em->flush();
        return $entity;


    }
    public function listUserAccounts() {
        $entities = $this->em->getRepository('TrackAccountBundle:Account')->findAll();
        $query = $this->em
            ->createQuery('
                SELECT ab FROM TrackAccountBundle:Account ab JOIN ab.roles r
                WHERE   r.role = :role
                '
            )->setParameters(array('role' => 'ROLE_USER'));
        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
       // return $entities ;
    }
    public function listUserAccountsByCompany($id) {
        $entities = $this->em->getRepository('TrackAccountBundle:Account')->findAll();
        $query = $this->em
            ->createQuery('
                SELECT ab FROM TrackAccountBundle:Account ab JOIN ab.roles r
                WHERE   r.role = :role and ab.company = :company
                '
            )->setParameters(array('role' => 'ROLE_USER', 'company' => $id));
        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
	private function matchPasswords($newPassword ,$oldPassword ,  $encoder, $salt) {
        $encodePassword =  $this->encodePassword($newPassword, $encoder , $salt);
        return StringUtils::equals($oldPassword, $encodePassword);
    }

    private function encodePassword( $plainTextPassword, $encoder, $salt) {
        return $encoder->encodePassword($plainTextPassword,$salt);
    }
    

}