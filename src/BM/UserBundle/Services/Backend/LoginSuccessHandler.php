<?php 
 
namespace Track\AccountBundle\Services\Backend; 
 
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface; 
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface; 
use Symfony\Component\HttpFoundation\Request; 
use Symfony\Component\HttpFoundation\RedirectResponse; 
use Symfony\Component\Routing\Router; 
use Symfony\Component\Security\Core\SecurityContext;
 
class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface { 

    protected $router; 
    protected $security; 
   
    public function __construct(Router $router, SecurityContext $security) { 
      $this->router = $router; 
      $this->security = $security; 
    } 
   
    public function onAuthenticationSuccess(Request $request, TokenInterface $token) { 
      if (count($this->security->getToken()->getRoles()) == 0 ) {
        die ('something wrong you have no role ');
      }

      if ($this->security->isGranted('ROLE_SUPER_ADMIN')) {
        if ($this->security->getToken()->getUser()->getLanguage() != '')
          $locale = $this->security->getToken()->getUser()->getLanguage();
        else
          $locale = $request->getLocale(); 
        $session = $request->getSession();
        $companyListSession = $session->get('companyListSession');

        if (!empty($companyListSession))
          $time = $companyListSession['time'];
        else
          $time = '';
        $response = new RedirectResponse($this->router->generate('backend_company_list', array('_locale' => $locale, 'time' =>$time)));
      }
      elseif ($this->security->isGranted('ROLE_ADMIN') || $this->security->isGranted('ROLE_USER')) {
        if ($this->security->getToken()->getUser()->getLanguage() != '')
          $locale = $this->security->getToken()->getUser()->getLanguage();
        else
          $locale = $request->getLocale();
        $company = $this->security->getToken()->getUser()->getCompany();
        if ($company != NULL) {
          $campaigns = $company->getCampaigns();
          if (!empty($campaigns) && $campaigns[0] != '') {
            $firstCampaign = $campaigns[0];
            $id = $firstCampaign->getId();
            $channels = $firstCampaign->getChannels();
            if (!empty($channels) && $channels[0] != '') {
              $firstChannel = $channels[0];
              $id = $firstChannel->getId();
              // $meduims = $firstChannel->getMediums();
              // if (!empty($meduims) && $meduims[0] != '') {
              //   $firstMeduim = $meduims[0];
              //   $id = $firstMeduim->getId();
              //   $response = new RedirectResponse($this->router->generate('backend_medium_view', array('_locale' => $locale,'id' => $id)));
              // }
              // else {
                $response = new RedirectResponse($this->router->generate('backend_channel_view', array('_locale' => $locale,'id' => $id)));
              //}
            }
            else {
              $response = new RedirectResponse($this->router->generate('backend_campaign_view', array('_locale' => $locale,'id' => $id)));
            }
          }
          else {
            $response = new RedirectResponse($this->router->generate('backend_company_overview', array('_locale' => $locale)));
          }
        }
        else {
        $response = new RedirectResponse($this->router->generate('backend_account_overview', array('_locale' => $locale)));
        }
      } 
      // elseif ($this->security->isGranted('ROLE_USER')) {
      //   if ($this->security->getToken()->getUser()->getLanguage() != '')
      //     $locale = $this->security->getToken()->getUser()->getLanguage();
      //   else
      //     $locale = $request->getLocale();
      //   // redirect the user to where they were before the login process begun.
      //   //$referer_url = $request->headers->get('referer');
      //   $response = new RedirectResponse($this->router->generate('backend_account_overview', array('_locale' => $locale)));
      //   //$response = new RedirectResponse($referer_url);
      // }
      return $response;
    } 
   
}