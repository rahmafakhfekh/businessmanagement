<?php
namespace Track\AccountBundle\Services\Backend;
use Track\Classes\AnnotationControllerAbstract;
class BillingAdressServices {

	private $em ;
    private $securityContext;
    private $container;
	public function __construct($em, $securityContext, $container){
        $this->em = $em;
        $this->securityContext = $securityContext;
        $this->container = $container;
	}
	public function newEntityBillingAdress(){
        $entity = $this->container->get('billingAdress.factory')->createOne();
        return $entity ;
    }
    public function createBillingAdress($id){
		$account = $this->em->getRepository('TrackAccountBundle:Account')->find( $id);
        $entity = $this->em->getRepository('TrackAccountBundle:BillingAdress')->findOneBy(array('account' => $account) );
        if ($entity == NULL) {
        	$entity = $this->newEntityBillingAdress();
	       	// $entity->setLegalPerson('legal');
	       	// $entity->setSaluation('male');
	       	// $entity->setContactMe('me');
	       	// $entity->setAccount($account);
        }
        return $entity ;
	}
	

}