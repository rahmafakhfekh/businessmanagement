<?php

namespace BM\UserBundle\DataFixtures\ORM;

use BM\UserBundle\Entity\User;
use BM\UserBundle\Entity\Role;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;


class LoadUserData extends AbstractFixture implements ContainerAwareInterface
{
	 use ContainerAwareTrait;

	public function load(ObjectManager $manager){

		$passwordEncoder = $this->container->get('security.password_encoder');
		$role = new Role();
		$role
			->setName('admin')
			->setRole('ROLE_ADMIN');
		$manager->persist($role);
		$role2 = new Role();
		$role2
			->setName('provider')
			->setRole('ROLE_PROVIDER');
		$manager->persist($role2);
		$role3 = new Role();
		$role3
			->setName('customer')
			->setRole('ROLE_CUSTOMER');
		$manager->persist($role3);
		//$role = $manager->getRepository('TrackAccountBundle:Role')->find(14);
		$basicUser = new User();
		$encodedPassword = $passwordEncoder->encodePassword($basicUser, 'bacem');
		$basicUser
			->setEmail('bacem@gmail.com')
			->setPassword($encodedPassword)
			->setUsername('bacem');
		$basicUser	->setFirstName('rahma')
			->setLastName('fakhfekh')
			
			->setActivationToken('abaadkflsfsfl8898wedsofls')
			
			->setSubscriptionDate(new \DateTime('NOW'));
		$basicUser->addRole($role);
		$role->addUser($basicUser);
		$manager->persist($basicUser);

		// $basicUser2 = new User();
		// $basicUser2->setSalt('fatma');
		// $encodedPassword2 = $passwordEncoder->encodePassword($basicUser2, 'fatma');
		// $basicUser2
		// 	->setEmail('fatma@gmail.com')
		// 	->setFirstName('fatma')
		// 	->setLastName('fatma')
		// 	->setPassword($encodedPassword)
		// 	->setActivationToken('abaadkflsfsfl8898wedsofls')
		// 	->setIsActive(true)
		// 	->setSubscriptionDate(new \DateTime('NOW'));
		// $basicUser2->addRole($role);
		// $role->addUser($basicUser2);
		// $manager->persist($basicUser2);
		// $manager->persist($role);
		$manager->flush();

	}
}