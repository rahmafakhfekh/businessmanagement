<?php

namespace Track\AccountBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Track\Classes\FormTypeFactoryTypeAbstract;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Track\CompanyBundle\Form\DataTransformer\StringToBooleanTransformer;

class BillingAdressType extends FormTypeFactoryTypeAbstract
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('saluation', 'choice', array(
                'choices'   => array('male' => 'Mr.', 'female' => 'Mrs.'),
                'required'  => true,
                'attr' => array('class'=>'form-control','label'=>false),
                'multiple'  => false,
                'expanded'  => true,
                'label'=>false,
                'mapped' => true))
            ->add('firstName', 'text', array(
                'required'=>true,
                'attr'=>array('placeholder'=>'billingAdress.settings.firstName')))
            ->add('lastName','text', array(
                'required'=>true,'attr'=>array('placeholder'=>'billingAdress.settings.lastName')))
            ->add('companyName', 'text', array('attr'=>array('placeholder'=>'billingAdress.settings.companyName')))
            ->add('legalForm', 'text', array('attr'=>array('placeholder'=>'billingAdress.settings.legalForm')))
            ->add('street', 'text', array('attr'=>array('placeholder'=>'billingAdress.settings.street')))
            ->add('number', 'text', array(
                'required'=>true,'attr'=>array('placeholder'=>'billingAdress.settings.number')))
            ->add('additive', 'text', array(
                'required'=>true,'attr'=>array('placeholder'=>'billingAdress.settings.additive')))
            ->add('postCode','text', array(
                'required'=>true,'attr'=>array('placeholder'=>'billingAdress.settings.postCode')))
            ->add('city', 'choice', array(
                'choices'   => array('Istanbul' => 'Istanbul', 'Ankara' => 'Ankara', 'Izmir' => 'Izmir', 'Bursa' => 'Bursa'),
                'required'  => true,
                'attr'=>array('class'=>'form-control','label'=>false),
                'empty_value' => 'billingAdress.settings.selectCity',
                'label'=>false))
            ->add('country', 'choice', array(
                'choices'   => array('Acigöl' => 'Acigöl', 'Adakli' => 'Adakli', 'Adilcevaz' => 'Adilcevaz', 'Ağaçören' => 'Ağaçören'),
                'required'  => true,
                'attr'=>array('class'=>'form-control'),
                'empty_value' => 'billingAdress.settings.selectCountry',
                'label'=>false ))
            ->add('legalPerson', 'choice', array(
                'choices'   => array('legal' => 'billingAdress.settings.legalPerson', 
                'natural' => 'billingAdress.settings.naturalPerson'),
                'required'  => true,
                'attr'=>array('class'=>'form-control','label'=>false),
                'multiple'  => false,
                'expanded'  => true,
                'label'=>false,
                'mapped' => true  ))
           
            ->add('idNumber', 'text', array( 
                'pattern'=>'\d*', 
                'attr'=>array('data-legal'=>'taxIdOrNumber','placeholder'=>'billingAdress.settings.idNumber')))
            ->add('taxNumber', 'text', array(
                'attr'=>array('data-legal'=>'taxIdOrNumber','placeholder'=>'billingAdress.settings.taxNumber'),
                'required'  => true))
            ->add('taxOffice', 'text', array('attr'=>array('placeholder'=>'billingAdress.settings.taxOffice')))
            ->add('firstNameContact', 'text', array('attr'=>array('placeholder'=>'billingAdress.settings.firstNameContact')))
            ->add('lastNameContact', 'text', array('attr'=>array('placeholder'=>'billingAdress.settings.lastNameContact')))
            ->add('phoneNumberContact', 'text', array(
                'required'=>false,'attr'=>array('placeholder'=>'billingAdress.settings.phoneNumberContact')))
            ->add('emailContact', 'text', array(
                'required'=>false,'attr'=>array('placeholder'=>'billingAdress.settings.emailContact')))
            ->add('contactMe', 'choice', array(
                'choices'   => array('me' => 'billingAdress.settings.me', 'another' => 'billingAdress.settings.another'),
                'required'  => true,
                'attr'=>array('class'=>'form-control','label'=>false),
                'multiple'  => false,
                'expanded'  => true,
                'label'=>false,
                'mapped' => true))
            ;
        ;
    }
    
    public function getName(){
        return 'BillingAdress';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver){
        $resolver -> setDefaults($this -> getDefaultOptions(array()));
    }
    
    public function getDefaultOptions(array $options){
        return array(
                'data_class' => 'Track\AccountBundle\Entity\BillingAdress',
        );
    }
}
