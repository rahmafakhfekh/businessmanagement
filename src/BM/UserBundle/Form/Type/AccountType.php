<?php 

namespace Track\AccountBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Track\Classes\FormTypeFactoryTypeAbstract;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AccountType extends FormTypeFactoryTypeAbstract{
	
	public function buildForm(FormbuilderInterface $builder, array $options){
		
		
		$builder
			->add('email', 'text', array('attr'=>array('placeholder'=>'account.settings.email')))
            ->add('name', 'text', array('attr'=>array('placeholder'=>'account.settings.name')))
            //->add('lastName', 'text', array('attr'=>array('placeholder'=>'account.settings.lastName')))
            ->add('password', 'password', array('attr'=>array('placeholder'=>'account.settings.password')))
            ->add('phoneNumber', 'text', array('attr'=>array('class'=>'tooltipTelx', 
            	'placeholder' =>'account.settings.phone'), 'required'  => false))
            ->add('currentPassword', 'password', array("mapped"=>false,'required'  => false,
            	'attr'=>array('placeholder'=>'account.settings.currentPassword')))
            ->add('newPassword','password',array('mapped'=>false,'required'  => false,
            	'attr'=>array('placeholder'=>'account.settings.newPassword')))
            ->add('newRetypedPassword','password',array('mapped'=>false,'required'  => false,
            	'attr'=>array('placeholder'=>'account.settings.retypePassword')))
            ->add('language', 'choice', array('choices'=> array('en' => 'account.settings.language.en',
                'de' => 'account.settings.language.de', 'tr' => 'account.settings.language.tr'),
                'required'  => true,'attr'=>array()))
            ->add('timezone', 'choice', array('choices'=> array('de' => 'account.settings.timezone.de',
                'tr' => 'account.settings.timezone.tr'),
                'required'  => true,'attr'=>array()))	
             ->add('company', 'entity', array(
					'class'    => 'Track\CompanyBundle\Entity\Company',
					'attr'=>array('class'=>"chosen-select"),
					'label' => 'Company',
					'empty_value' => 'Choose Company',
					'property' => 'name',
					'multiple' => false,
					'required'=>true,
				))	;		
		;
	}
	
	public function getName(){
		return 'Account';
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver){
		$resolver -> setDefaults($this -> getDefaultOptions(array()));
	}
	
	public function getDefaultOptions(array $options){
		return array(
				'data_class' => 'Track\AccountBundle\Entity\Account',
		);
	}
}