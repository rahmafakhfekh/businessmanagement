<?php

namespace BM\SalesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use BM\SalesBundle\Form\ArticleDocumentType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
class SalesDocumentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('numDoc')->add('dateCreation')->add('dateReglement')->add('remise')->add('totalHT')->add('totalHTNet')->add('montantTVA')->add('totalTTC')->add('typeDocument')->add('client')
        ->add('dateCreation', DateType::class, array(
            //'widget' => 'single_text',
            // this is actually the default format for single_text
           // 'format' => 'yyyy-MM-dd',
            'data' => new \DateTime()
        ))
        ->add('articleDocuments', CollectionType::class, array(
            'entry_type' => ArticleDocumentType::class,
             //       'type'  => new ArticleDocumentType(),
                 'allow_add'    => true,
                 'allow_delete' => true,
                 
               )
             )
        ->add('saleType', ChoiceType::class, array(
            'choices'  => array(
                'Detail' => 'detail',
                'Gros' => 'gros'            ),
        ));
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BM\SalesBundle\Entity\SalesDocument'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'bm_salesbundle_SalesDocument';
    }


}
