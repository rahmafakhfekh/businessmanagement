<?php

namespace BM\SalesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use BM\SalesBundle\Entity\SalesDocument;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TypeDoc
 *
 * @ORM\Table(name="type_doc")
 * @ORM\Entity(repositoryClass="BM\SalesBundle\Repository\TypeDocRepository")
 */
class TypeDoc
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;



     /**
    * @ORM\OneToMany(targetEntity="BM\SalesBundle\Entity\SalesDocument", mappedBy="typeDocument")
    */
   private $documents;
    public function __construct(){
       $this-> documents = new ArrayCollection();
       
   }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return TypeDoc
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }





     /**
    * 
    * addDocument()
    */
   public function addDocument(SalesDocument $document ){
       $this->documents->add($document);
       return $this;
   }
   /**
    * 
    * removeDocument()
    */
   public function removeDocument(SalesDocument $document ){
       $this->documents->removeDocument($document);
       return $this;
   }

   /**
    * 
    * getDocuments()
    */
   public function getDocuments(){
       return $this->documents;
   }
   public function __toString() {
    return $this->libelle;
    }
}

