<?php

namespace BM\SalesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use BM\ArticleBundle\Entity\Article;
use BM\SalesBundle\Entity\SalesDocument;

/**
 * ArticleDocument
 *
 * @ORM\Table(name="article_document")
 * @ORM\Entity(repositoryClass="BM\SalesBundle\Repository\ArticleDocumentRepository")
 */
class ArticleDocument
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="quantite", type="string", length=255)
     */
    private $quantite;
    /**
     * @var string
     *
     * @ORM\Column(name="remise", type="string", length=255)
     */
    private $remise;
    /**
    * @ORM\ManyToOne(targetEntity="BM\SalesBundle\Entity\SalesDocument" , inversedBy="articleDocuments")
    * @ORM\JoinColumn(name="salesDocument_id", referencedColumnName="id",onDelete="SET NULL" )
    */
   private $salesDocument;


   /**
    * @ORM\ManyToOne(targetEntity="BM\ArticleBundle\Entity\Article" , inversedBy="articleDocuments")
    * @ORM\JoinColumn(name="article_id", referencedColumnName="id",onDelete="SET NULL" )
    */
   private $article;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantite
     *
     * @param string $quantite
     *
     * @return ArticleDocument
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return string
     */
    public function getQuantite()
    {
        return $this->quantite;
    }




  /**
     * Set salesDocument
     *
     * @param string $salesDocument
     *
     * @return ArticleDocument
     */
    public function setSalesDocument($salesDocument)
    {
        $this->salesDocument = $salesDocument;

        return $this;
    }

    /**
     * Get docvents
     *
     * @return string
     */
    public function getSalesDocument()
    {
        return $this->salesDocument;
    }



  /**
     * Set art
     *
     * @param string $art
     *
     * @return ArticleDocument
     */
    public function setArticle($article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get art
     *
     * @return string
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set remise
     *
     * @param string $remise
     *
     * @return ArticleDocument
     */
    public function setRemise($remise)
    {
        $this->remise = $remise;

        return $this;
    }

    /**
     * Get remise
     *
     * @return string
     */
    public function getRemise()
    {
        return $this->remise;
    }
    

}

