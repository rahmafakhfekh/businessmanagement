<?php

namespace BM\SalesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use BM\SalesBundle\Entity\TypeDoc;
use BM\SalesBundle\Entity\ArticleDocument;
use BM\ClientBundle\Entity\Client;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * SalesDocument
 *
 * @ORM\Table(name="document_vent")
 * @ORM\Entity(repositoryClass="BM\SalesBundle\Repository\SalesDocumentRepository")
 */
class SalesDocument
{
    const DEVIS = 'quotation' ; 
    const BL = 'delivery';
    const INVOICE = 'invoice';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numDoc", type="string", length=255)
     */
    private $numDoc;

    /**
     * @var string
     *
     * @ORM\Column(name="dateCreation", type="date")
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateReglement", type="date")
     */
    private $dateReglement;

    /**
     * @var string
     *
     * @ORM\Column(name="remise", type="string", length=255)
     */
    private $remise;

    /**
     * @var string
     *
     * @ORM\Column(name="totalHT", type="string", length=255)
     */
    private $totalHT;

    /**
     * @var string
     *
     * @ORM\Column(name="totalHTNet", type="string", length=255)
     */
    private $totalHTNet;

    /**
     * @var string
     *
     * @ORM\Column(name="montantTVA", type="string", length=255)
     */
    private $montantTVA;

    /**
     * @var string
     *
     * @ORM\Column(name="totalTTC", type="string", length=255)
     */
    private $totalTTC;



    /**
     * @var string
     *
     * @ORM\Column(name="typeDocument", type="string", length=255)
     */
   private $typeDocument;



    /**
    * @ORM\ManyToOne(targetEntity="BM\ClientBundle\Entity\Client" , inversedBy="salesDocuments")
    * @ORM\JoinColumn(name="client_id", referencedColumnName="id",onDelete="SET NULL" )
    */
   private $client;
   /**
     * @var string
     *
     * @ORM\Column(name="saleType", type="string", length=255)
     */
    private $saleType;


   /**
    * @ORM\OneToMany(targetEntity="BM\SalesBundle\Entity\ArticleDocument", mappedBy="salesDocument",cascade={"persist","remove"})
    */
   private $articleDocuments;
    public function __construct(){
       $this-> articleDocuments = new ArrayCollection();
       
   }




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numDoc
     *
     * @param string $numDoc
     *
     * @return SalesDocument
     */
    public function setNumDoc($numDoc)
    {
        $this->numDoc = $numDoc;

        return $this;
    }

    /**
     * Get numDoc
     *
     * @return string
     */
    public function getNumDoc()
    {
        return $this->numDoc;
    }

    /**
     * Set dateCreation
     *
     * @param string $dateCreation
     *
     * @return SalesDocument
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateReglement
     *
     * @param \DateTime $dateReglement
     *
     * @return SalesDocument
     */
    public function setDateReglement($dateReglement)
    {
        $this->dateReglement = $dateReglement;

        return $this;
    }

    /**
     * Get dateReglement
     *
     * @return \DateTime
     */
    public function getDateReglement()
    {
        return $this->dateReglement;
    }

    /**
     * Set remise
     *
     * @param string $remise
     *
     * @return SalesDocument
     */
    public function setRemise($remise)
    {
        $this->remise = $remise;

        return $this;
    }

    /**
     * Get remise
     *
     * @return string
     */
    public function getRemise()
    {
        return $this->remise;
    }

    /**
     * Set totalHT
     *
     * @param string $totalHT
     *
     * @return SalesDocument
     */
    public function setTotalHT($totalHT)
    {
        $this->totalHT = $totalHT;

        return $this;
    }

    /**
     * Get totalHT
     *
     * @return string
     */
    public function getTotalHT()
    {
        return $this->totalHT;
    }

    /**
     * Set totalHTNet
     *
     * @param string $totalHTNet
     *
     * @return SalesDocument
     */
    public function setTotalHTNet($totalHTNet)
    {
        $this->totalHTNet = $totalHTNet;

        return $this;
    }

    /**
     * Get totalHTNet
     *
     * @return string
     */
    public function getTotalHTNet()
    {
        return $this->totalHTNet;
    }

    /**
     * Set montantTVA
     *
     * @param string $montantTVA
     *
     * @return SalesDocument
     */
    public function setMontantTVA($montantTVA)
    {
        $this->montantTVA = $montantTVA;

        return $this;
    }

    /**
     * Get montantTVA
     *
     * @return string
     */
    public function getMontantTVA()
    {
        return $this->montantTVA;
    }

    /**
     * Set totalTTC
     *
     * @param string $totalTTC
     *
     * @return SalesDocument
     */
    public function setTotalTTC($totalTTC)
    {
        $this->totalTTC = $totalTTC;

        return $this;
    }

    /**
     * Get totalTTC
     *
     * @return string
     */
    public function getTotalTTC()
    {
        return $this->totalTTC;
    }

  /**
     * Set typeDocument
     *
     * @param string $typeDocument
     *
     * @return SalesDocument
     */
    public function setTypeDocument($typeDocument)
    {
        $this->typeDocument = $typeDocument;

        return $this;
    }

    /**
     * Get typeDocument
     *
     * @return string
     */
    public function getTypeDocument()
    {
        return $this->typeDocument;
    }





      /**
     * Set client
     *
     * @param string $client
     *
     * @return SalesDocument
     */
    public function setClient(Client $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }









/**
    * 
    * addArtDoc()
   
    */
   public function addArticleDocument(ArticleDocument $articleDocument ){
       $this-> articleDocumentss->add($articleDocument);
       return $this;
   }
   /**
    * 
    * removeArtDoc()
    */
   public function removeArticleDocument(ArticleDocument $articleDocument ){
       $this->articleDocuments->removeElement($articleDocument);
       return $this;
   }

   /**
    * 
    * getArtDocs()
    */
   public function getArticleDocuments(){
       return $this->articleDocuments;
   }
   public function __toString() {
    return $this->id;
    }

    


    /**
     * Set saleType
     *
     * @param string $saleType
     *
     * @return SalesDocument
     */
    public function setSaleType($saleType)
    {
        $this->saleType = $saleType;

        return $this;
    }

    /**
     * Get saleType
     *
     * @return string
     */
    public function getSaleType()
    {
        return $this->saleType;
    }




 

    
}

