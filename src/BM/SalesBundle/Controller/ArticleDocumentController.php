<?php

namespace BM\SalesBundle\Controller;

use BM\SalesBundle\Entity\ArticleDocument;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Articledocument controller.
 *
 * @Route("articledocument")
 */
class ArticleDocumentController extends Controller
{
    /**
     * Lists all articleDocument entities.
     *
     * @Route("/", name="articledocument_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $articleDocuments = $em->getRepository('BMSalesBundle:ArticleDocument')->findAll();

        return $this->render('articledocument/index.html.twig', array(
            'articleDocuments' => $articleDocuments,
        ));
    }

    /**
     * Creates a new articleDocument entity.
     *
     * @Route("/new", name="articledocument_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $articleDocument = new Articledocument();
        $form = $this->createForm('BM\SalesBundle\Form\ArticleDocumentType', $articleDocument);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($articleDocument);
            $em->flush();

            return $this->redirectToRoute('articledocument_show', array('id' => $articleDocument->getId()));
        }

        return $this->render('articledocument/new.html.twig', array(
            'articleDocument' => $articleDocument,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a articleDocument entity.
     *
     * @Route("/{id}", name="articledocument_show")
     * @Method("GET")
     */
    public function showAction(ArticleDocument $articleDocument)
    {
        $deleteForm = $this->createDeleteForm($articleDocument);

        return $this->render('articledocument/show.html.twig', array(
            'articleDocument' => $articleDocument,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing articleDocument entity.
     *
     * @Route("/{id}/edit", name="articledocument_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ArticleDocument $articleDocument)
    {
        $deleteForm = $this->createDeleteForm($articleDocument);
        $editForm = $this->createForm('BM\SalesBundle\Form\ArticleDocumentType', $articleDocument);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('articledocument_edit', array('id' => $articleDocument->getId()));
        }

        return $this->render('articledocument/edit.html.twig', array(
            'articleDocument' => $articleDocument,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a articleDocument entity.
     *
     * @Route("/{id}", name="articledocument_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ArticleDocument $articleDocument)
    {
        $form = $this->createDeleteForm($articleDocument);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($articleDocument);
            $em->flush();
        }

        return $this->redirectToRoute('articledocument_index');
    }

    /**
     * Creates a form to delete a articleDocument entity.
     *
     * @param ArticleDocument $articleDocument The articleDocument entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ArticleDocument $articleDocument)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('articledocument_delete', array('id' => $articleDocument->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
