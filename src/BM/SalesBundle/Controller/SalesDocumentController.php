<?php

namespace BM\SalesBundle\Controller;

use BM\SalesBundle\Entity\SalesDocument;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * SalesDocument controller.
 *
 * @Route("salesDocument")
 */
class SalesDocumentController extends Controller
{
    /**
     * Lists all SalesDocument entities.
     *
     * @Route("/", name="salesDocument_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $SalesDocuments = $em->getRepository('BMSalesBundle:SalesDocument')->findAll();

        return $this->render('salesDocument/index.html.twig', array(
            'SalesDocuments' => $SalesDocuments,
        ));
    }

    /**
     * Creates a new SalesDocument entity.
     *
     * @Route("/new", name="salesDocument_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $salesDocument = new SalesDocument();
        $form = $this->createForm('BM\SalesBundle\Form\SalesDocumentType', $salesDocument);
        $form->handleRequest($request);
        //$salesDocument->setNumDoc($this->setDocumentNumber());
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            foreach ($salesDocument->getArticleDocuments() as $key => $articleDoc) {
                if ($articleDoc != NULL) {
                    $articleDoc->setSalesDocument($salesDocument);
                    $em->persist($articleDoc);
                }
            }

            $em->persist($salesDocument);
            $em->flush();

            return $this->redirectToRoute('salesDocument_show', array('id' => $salesDocument->getId()));
        }

        return $this->render('salesDocument/new.html.twig', array(
            'SalesDocument' => $salesDocument,
            'form' => $form->createView(),
            'docNumber' =>$this->setDocumentNumber()
        ));
    }

    /**
     * Finds and displays a SalesDocument entity.
     *
     * @Route("/{id}", name="salesDocument_show")
     * @Method("GET")
     */
    public function showAction(SalesDocument $SalesDocument)
    {
        $deleteForm = $this->createDeleteForm($SalesDocument);

        return $this->render('salesDocument/show.html.twig', array(
            'SalesDocument' => $SalesDocument,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing SalesDocument entity.
     *
     * @Route("/{id}/edit", name="salesDocument_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, SalesDocument $SalesDocument)
    {
        $deleteForm = $this->createDeleteForm($SalesDocument);
        $editForm = $this->createForm('BM\SalesBundle\Form\SalesDocumentType', $SalesDocument);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            foreach ($SalesDocument->getArticleDocuments() as $key => $articleDoc) {
                if ($articleDoc != NULL) {
                    $articleDoc->setSalesDocument($SalesDocument);
                    $em->persist($articleDoc);
                }
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('salesDocument_edit', array('id' => $SalesDocument->getId()));
        }

        return $this->render('salesDocument/edit.html.twig', array(
            'SalesDocument' => $SalesDocument,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a SalesDocument entity.
     *
     * @Route("/{id}", name="salesDocument_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, SalesDocument $SalesDocument)
    {
        $form = $this->createDeleteForm($SalesDocument);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($SalesDocument);
            $em->flush();
        }

        return $this->redirectToRoute('salesDocument_index');
    }

    /**
     * Creates a form to delete a SalesDocument entity.
     *
     * @param SalesDocument $SalesDocument The SalesDocument entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SalesDocument $SalesDocument)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('salesDocument_delete', array('id' => $SalesDocument->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    public function setDocumentNumber() {
        $em = $this->getDoctrine()->getManager();
        $devis = $em->getRepository('BMSalesBundle:SalesDocument')->findBy(array('typeDocument' => SalesDocument::BL));
        $format = count($devis)+1;
        if($format >=1 and $format <10){
            $documentNumber = 'D-00'.$format.'/'.date("Y");
            
        }elseif ($format >=10 and $format <=99) {
            $documentNumber = 'D-0'.$format.'/'.date("Y");
        }else{
            $documentNumber = 'D-'.$format.'/'.date("Y");
        }
        return $documentNumber;
    }
}
