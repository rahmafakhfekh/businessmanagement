<?php

namespace BM\SalesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BMSalesBundle:Default:index.html.twig');
    }
}
