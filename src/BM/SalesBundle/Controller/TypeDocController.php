<?php

namespace BM\SalesBundle\Controller;

use BM\SalesBundle\Entity\TypeDoc;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Typedoc controller.
 *
 * @Route("typedoc")
 */
class TypeDocController extends Controller
{
    /**
     * Lists all typeDoc entities.
     *
     * @Route("/", name="typedoc_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $typeDocs = $em->getRepository('BMSalesBundle:TypeDoc')->findAll();

        return $this->render('typedoc/index.html.twig', array(
            'typeDocs' => $typeDocs,
        ));
    }

    /**
     * Creates a new typeDoc entity.
     *
     * @Route("/new", name="typedoc_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $typeDoc = new Typedoc();
        $form = $this->createForm('BM\SalesBundle\Form\TypeDocType', $typeDoc);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typeDoc);
            $em->flush();

            return $this->redirectToRoute('typedoc_show', array('id' => $typeDoc->getId()));
        }

        return $this->render('typedoc/new.html.twig', array(
            'typeDoc' => $typeDoc,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a typeDoc entity.
     *
     * @Route("/{id}", name="typedoc_show")
     * @Method("GET")
     */
    public function showAction(TypeDoc $typeDoc)
    {
        $deleteForm = $this->createDeleteForm($typeDoc);

        return $this->render('typedoc/show.html.twig', array(
            'typeDoc' => $typeDoc,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing typeDoc entity.
     *
     * @Route("/{id}/edit", name="typedoc_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TypeDoc $typeDoc)
    {
        $deleteForm = $this->createDeleteForm($typeDoc);
        $editForm = $this->createForm('BM\SalesBundle\Form\TypeDocType', $typeDoc);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('typedoc_edit', array('id' => $typeDoc->getId()));
        }

        return $this->render('typedoc/edit.html.twig', array(
            'typeDoc' => $typeDoc,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a typeDoc entity.
     *
     * @Route("/{id}", name="typedoc_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TypeDoc $typeDoc)
    {
        $form = $this->createDeleteForm($typeDoc);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typeDoc);
            $em->flush();
        }

        return $this->redirectToRoute('typedoc_index');
    }

    /**
     * Creates a form to delete a typeDoc entity.
     *
     * @param TypeDoc $typeDoc The typeDoc entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TypeDoc $typeDoc)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('typedoc_delete', array('id' => $typeDoc->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
