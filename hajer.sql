-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 11 Mai 2018 à 10:42
-- Version du serveur :  5.6.31-0ubuntu0.15.10.1
-- Version de PHP :  5.6.11-1ubuntu3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `hajer`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `tva_id` int(11) DEFAULT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PriceVDHT` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PriceVDTTC` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priceVGHT` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PriceVGTTC` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PriceAHT` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PriceATTC` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Cut` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `height` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `width` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `length` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barCode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qt` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `article`
--

INSERT INTO `article` (`id`, `category_id`, `tva_id`, `reference`, `designation`, `PriceVDHT`, `PriceVDTTC`, `priceVGHT`, `PriceVGTTC`, `PriceAHT`, `PriceATTC`, `Status`, `Cut`, `color`, `height`, `width`, `length`, `weight`, `barCode`, `qt`) VALUES
(1, 3, 4, 'aaaa', 'pantalon', '35', '39.9', '25', '28.5', '20', '22.8', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'aaa', '25'),
(2, 3, 1, 'bbbb', 'bbbb', '89', '90.78', '56', '57.12', '52', '53.04', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'bbb', '87'),
(3, 3, 6, 'ccc', 'ccc', '25', '31.25', '20', '25', '15', '18.75', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ccc', '23'),
(4, 3, NULL, 'rr', 'rr', '32', '32', '21', '21', '14', '14', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'rr', '96');

-- --------------------------------------------------------

--
-- Structure de la table `article_document`
--

CREATE TABLE IF NOT EXISTS `article_document` (
  `id` int(11) NOT NULL,
  `article_id` int(11) DEFAULT NULL,
  `quantite` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `salesDocument_id` int(11) DEFAULT NULL,
  `remise` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `article_document`
--

INSERT INTO `article_document` (`id`, `article_id`, `quantite`, `salesDocument_id`, `remise`) VALUES
(1, 1, '2', NULL, ''),
(2, 2, '3', NULL, ''),
(3, 1, '2', 2, ''),
(4, 2, '3', 2, ''),
(5, 1, '2', 3, '3'),
(6, 3, '3', 3, '1'),
(7, 2, '5', NULL, '0'),
(8, 2, '5', 3, '0'),
(9, 4, '6', 3, '0');

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'esthetique'),
(2, 'tissu'),
(3, 'vetement'),
(4, 'nourriture');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) NOT NULL,
  `numcl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nomcl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `raisonsocialcl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adressecl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `codepostalcl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `villecl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payscl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telcl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emailcl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `matriculeFiscalCl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NExonerationFiscal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `codeDouaneCl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ExoTimbreFiscalCl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TypecommercialeCl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remiseCl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `faxCl` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`id`, `numcl`, `nomcl`, `raisonsocialcl`, `adressecl`, `codepostalcl`, `villecl`, `payscl`, `telcl`, `emailcl`, `matriculeFiscalCl`, `NExonerationFiscal`, `codeDouaneCl`, `ExoTimbreFiscalCl`, `TypecommercialeCl`, `remiseCl`, `faxCl`) VALUES
(1, 'aaaaa', 'rahma', 'ingénieur', 'rte gremda', '254', 'sfax', 'tunisie', '20478102', 'rahmafakhfekh@gmail.com', 'zzz', 'zz', 'zzz', 'zz', 'personne physique', '2', 'zzz'),
(2, 'qqq', 'fatma', 'sdcdsf', 'azezaeze', 'zeze', 'zeze', 'ze', 'ze', 'zez', 'dfdf', 'dsfds', 'dsfdsq', 'dfsqdf', 'dfds', '3', 'e'),
(3, 'zzz', 'hajer', 'sd', 'sds', 'sd', 'sdsd', 'sd', 'sd', 'sds', 'dsd', 'sdsds', 'dsd', 'sd', 'sds', '0', 'd');

-- --------------------------------------------------------

--
-- Structure de la table `document_vent`
--

CREATE TABLE IF NOT EXISTS `document_vent` (
  `id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `numDoc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreation` date NOT NULL,
  `dateReglement` date NOT NULL,
  `remise` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `totalHT` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `totalHTNet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `montantTVA` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `totalTTC` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `saleType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `typeDocument` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `document_vent`
--

INSERT INTO `document_vent` (`id`, `client_id`, `numDoc`, `dateCreation`, `dateReglement`, `remise`, `totalHT`, `totalHTNet`, `montantTVA`, `totalTTC`, `saleType`, `typeDocument`) VALUES
(1, 1, 'D-001/2018', '2018-05-10', '2013-01-01', '19.34', '337', '317.66', '13.0732', '330.7332', 'detail', 'delivery'),
(2, 2, 'D-002/2018', '2018-05-10', '2013-01-01', '19.34', '337', '317.66', '13.0732', '330.7332', 'detail', 'delivery'),
(3, 3, 'D-003/2018', '2018-05-10', '2013-01-01', '2.1', '516', '513.9', '27.240000000000002', '541.14', 'gros', 'delivery');

-- --------------------------------------------------------

--
-- Structure de la table `provider`
--

CREATE TABLE IF NOT EXISTS `provider` (
  `id` int(11) NOT NULL,
  `codeFour` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nomFour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `raisonSocialFour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codPosFour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `villeFour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paysFour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telFour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emailFour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commantaireFour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faxFour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `siteWebFour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `matriculeFiscalFour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codDouaneFour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `stock`
--

CREATE TABLE IF NOT EXISTS `stock` (
  `id` int(11) NOT NULL,
  `article_id` int(11) DEFAULT NULL,
  `QT` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `stock`
--

INSERT INTO `stock` (`id`, `article_id`, `QT`) VALUES
(1, 1, '25'),
(2, 2, '87'),
(3, 3, '23'),
(4, 4, '96');

-- --------------------------------------------------------

--
-- Structure de la table `tva`
--

CREATE TABLE IF NOT EXISTS `tva` (
  `id` int(11) NOT NULL,
  `pourcentageTva` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `tva`
--

INSERT INTO `tva` (`id`, `pourcentageTva`) VALUES
(1, '2'),
(2, '5'),
(3, '7'),
(4, '14'),
(5, '18'),
(6, '25');

-- --------------------------------------------------------

--
-- Structure de la table `type_doc`
--

CREATE TABLE IF NOT EXISTS `type_doc` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_23A0E6612469DE2` (`category_id`),
  ADD KEY `IDX_23A0E664D79775F` (`tva_id`);

--
-- Index pour la table `article_document`
--
ALTER TABLE `article_document`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8D817E1D29D4B0F4` (`salesDocument_id`),
  ADD KEY `IDX_8D817E1D7294869C` (`article_id`);

--
-- Index pour la table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `document_vent`
--
ALTER TABLE `document_vent`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_2AAE560819EB6921` (`client_id`);

--
-- Index pour la table `provider`
--
ALTER TABLE `provider`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_4B3656607294869C` (`article_id`);

--
-- Index pour la table `tva`
--
ALTER TABLE `tva`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `type_doc`
--
ALTER TABLE `type_doc`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `article_document`
--
ALTER TABLE `article_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `document_vent`
--
ALTER TABLE `document_vent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `provider`
--
ALTER TABLE `provider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `tva`
--
ALTER TABLE `tva`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `type_doc`
--
ALTER TABLE `type_doc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `FK_23A0E6612469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_23A0E664D79775F` FOREIGN KEY (`tva_id`) REFERENCES `tva` (`id`) ON DELETE SET NULL;

--
-- Contraintes pour la table `article_document`
--
ALTER TABLE `article_document`
  ADD CONSTRAINT `FK_8D817E1D29D4B0F4` FOREIGN KEY (`salesDocument_id`) REFERENCES `document_vent` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_8D817E1D7294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE SET NULL;

--
-- Contraintes pour la table `document_vent`
--
ALTER TABLE `document_vent`
  ADD CONSTRAINT `FK_2AAE560819EB6921` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE SET NULL;

--
-- Contraintes pour la table `stock`
--
ALTER TABLE `stock`
  ADD CONSTRAINT `FK_4B3656607294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
